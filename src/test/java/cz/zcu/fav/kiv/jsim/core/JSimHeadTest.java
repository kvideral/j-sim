package cz.zcu.fav.kiv.jsim.core;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;

import java.util.UUID;

import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;

import cz.zcu.fav.kiv.jsim.core.exception.JSimInvalidParametersException;

/**
 * JSimHeadTest.java
 *    29. 10. 2015
 * @author Lukáš Kvídera
 * @version 1.0
 */
public class JSimHeadTest {

  private JSimHead head;
  private JSimSimulation simulation;

  @Before
  public void setUp() throws Exception {
    this.simulation = new JSimSimulation("Test simulatiuon", UUID.randomUUID());
    this.head = new JSimHead("Test head", this.simulation);
  }

  @Test
  public void testEmpty_IsEmpty_True() {
    assertTrue("Head is not empty!", this.head.empty());
  }

  @Test
  @Ignore("Issue #4")
  public void testEmpty_InsertFront_False() {
    this.head.putAtHead(new JSimLink());
    assertFalse("Head is empty!", this.head.empty());
  }

  @Test
  @Ignore("Issue #4")
  public void testEmpty_InsertBack_False() {
    this.head.putAtTail(new JSimLink());
    assertFalse("Head is empty!", this.head.empty());
  }

  // -----------------------------------------------------------

  @Test
  public void testCardinal_IsEmpty_0() {
    assertEquals("Head is not empty!", 0L, this.head.cardinal());
  }

  @Test
  @Ignore("Issue #4")
  public void testCardinal_InsertFront_1() {
    this.head.putAtHead(new JSimLink());
    assertEquals("Head is empty!", 1L, this.head.cardinal());
  }

  @Test
  @Ignore("Issue #4")
  public void testCardinal_InsertBack_1() {
    this.head.putAtTail(new JSimLink());
    assertEquals("Head is empty!", 1L, this.head.cardinal());
  }

  // -----------------------------------------------------------

  @Test
  public void testFirst_IsEmpty_Null() {
    assertNull("Head is not empty!", this.head.first());
  }

  @Test
  public void testFirst_InsertFront_BackLink() {
    final JSimLink jSimLink = new JSimLink();
    this.head.putAtHead(jSimLink);
    assertEquals("Back contains different link!", jSimLink, this.head.first());
  }

  @Test
  public void testFirst_InsertBack_BackLink() {
    final JSimLink jSimLink = new JSimLink();
    this.head.putAtTail(jSimLink);
    assertEquals("Head contains different link!", jSimLink, this.head.first());
  }

  // -----------------------------------------------------------

  @Test
  public void testLast_IsEmpty_Null() {
    assertNull("Tail is not empty!", this.head.last());
  }

  @Test
  public void testLast_InsertFront_FrontLink() {
    final JSimLink jSimLink = new JSimLink();
    this.head.putAtHead(jSimLink);
    assertEquals("Back contains different link!", jSimLink, this.head.last());
  }

  @Test
  public void testLast_InsertBack_FrontLink() {
    final JSimLink jSimLink = new JSimLink();
    this.head.putAtTail(jSimLink);
    assertEquals("Head contains different link!", jSimLink, this.head.last());
  }

  // -----------------------------------------------------------

  @Test(expected = JSimInvalidParametersException.class)
  public void testPutAtHead_InsertNull_ShouldThrowException() {
    this.head.putAtHead(null);
  }

  @Test(expected = JSimInvalidParametersException.class)
  public void testPutAtTail_InsertNull_ShouldThrowException() {
    this.head.putAtTail(null);
  }

  // -----------------------------------------------------------

  @Test
  public void testClear() throws Exception {
    for (int i = 0; i < 10; ++i) {
      new JSimLink().into(this.head);
    }

    assertEquals("Some JSimLink is missing!", 10L, this.head.cardinal());
    this.head.clear();
    assertEquals("Some JSimLink still remains!", 0L, this.head.cardinal());
  }

  // -----------------------------------------------------------

  @Test
  @Ignore
  public void getHeadNumber() throws Exception {
    assertEquals("Head is not first!", 0L, this.head.getHeadNumber());
    final JSimHead head = new JSimHead("Another", this.simulation);
    assertEquals("Head is not first!", 1L, head.getHeadNumber());
  }

  // -----------------------------------------------------------

  @Test
  public void getFirstData() {
    assertNull("Some data foud!", this.head.getFirstData());
    this.head.putAtHead(new JSimLink("data"));
    assertEquals("data", this.head.getFirstData());
  }

  @Test
  public void getFirstDataType() {
    assertNull("Some data foud!", this.head.getFirstDataType());
    this.head.putAtHead(new JSimLink("data"));
    assertEquals("java.lang.String", this.head.getFirstDataType());
  }

  // -----------------------------------------------------------

  @Test
  public void getLastData() {
    assertNull("Some data foud!", this.head.getLastData());
    this.head.putAtHead(new JSimLink("data"));
    assertEquals("data", this.head.getLastData());
  }

  @Test
  public void getLastDataType() {
    assertNull("Some data foud!", this.head.getLastDataType());
    this.head.putAtHead(new JSimLink("data"));
    assertEquals("java.lang.String", this.head.getLastDataType());
  }

  // -----------------------------------------------------------


  @Test
  @Ignore
  public void testCompareTo() throws Exception {
    final JSimHead h1 = this.head;
    final JSimHead h2 = new JSimHead("Second", this.simulation);

    assertEquals("Wrong order!", 0, h1.compareTo(h1));
    assertEquals("Wrong order!", -1, h1.compareTo(h2));
    assertEquals("Wrong order!", 1, h2.compareTo(h1));
  }

  @Test
  @Ignore
  public void testEquals() throws Exception {
    final JSimHead h1 = this.head;
    final JSimHead h2 = new JSimHead("Second", this.simulation);
    final JSimHead h3 = new JSimHead("First in another simulation",
        new JSimSimulation("Another simulation", UUID.randomUUID()));

    assertTrue("Queues are not equal!", h1.equals(h1));
    assertFalse("Queues are equal!", h1.equals(h2));
    assertFalse("Queues are equal!", h1.equals("Hello wolrd!"));
    assertFalse("Queues are equal!", h1.equals(h3));
  }

}
