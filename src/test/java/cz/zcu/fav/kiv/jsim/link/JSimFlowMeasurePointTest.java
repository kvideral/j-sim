package cz.zcu.fav.kiv.jsim.link;

import static org.junit.Assert.assertEquals;

import java.util.UUID;

import org.junit.Test;

import cz.zcu.fav.kiv.jsim.link.JSimTimeFactory.JSimTime;

/**
 * JSimMeanMeasurePointTest.java
 *    11. 12. 2015
 * @author Lukáš Kvídera
 * @version 0.0
 */
public class JSimFlowMeasurePointTest {

  @Test
  public void testAvg() {
    final UUID uuid = UUID.randomUUID();
    final JSimTime globalTime = JSimTimeFactory.getGlobalTime(uuid);
    JSimQueueFactoryManager.createNewFactory(uuid, globalTime);
    final JSimFlowMeasurePoint measurePoint = JSimFlowMeasurePointFactory.createMeasurePoint(
        uuid,
        "THE END",
        "/dev/null"
        );

    globalTime.setTime(2.0);
    measurePoint.accept(new JSimObject(globalTime));
    assertEquals("Invalid Mean!", 2.0, measurePoint.getMean(), 0.0);

    measurePoint.accept(new JSimObject(globalTime));
    assertEquals("Invalid Mean!", 1.0, measurePoint.getMean(), 0.0);


    globalTime.setTime(2.5);
    measurePoint.accept(new JSimObject(globalTime));
    assertEquals("Invalid Mean!", 0.8333333333, measurePoint.getMean(), 0.00001);

    globalTime.setTime(3.0);
    measurePoint.accept(new JSimObject(globalTime));
    assertEquals("Invalid Mean!", 0.75, measurePoint.getMean(), 0.0);
  }

}
