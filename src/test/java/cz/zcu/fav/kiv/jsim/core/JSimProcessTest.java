package cz.zcu.fav.kiv.jsim.core;

import static org.junit.Assert.assertEquals;

import java.util.UUID;

import org.junit.Before;
import org.junit.Test;

import cz.zcu.fav.kiv.jsim.process.SimpleProcess;

/**
 * JSimProcessTest.java
 *    30. 10. 2015
 * @author Lukáš Kvídera
 * @version 1.0
 */
public class JSimProcessTest {

  private JSimSimulation simulation;

  @Before
  public void setUp() {
    this.simulation = new JSimSimulation("Simulation", UUID.randomUUID());
    this.simulation.addProcess(new SimpleProcess("x"));
  }

  @Test
  public void testGetProcessState() throws Exception{
    final JSimProcess process = new SimpleProcess("P1");
    assertEquals(JSimProcessState.NEW, process.getProcessState());
    this.simulation.addProcess(process);
    assertEquals(JSimProcessState.SCHEDULED, process.getProcessState());
    for (int i = 0; (i < 7) && this.simulation.step(); i++) {  /* do nothing */  }
    assertEquals(JSimProcessState.SCHEDULED, process.getProcessState());
  }
}
