package cz.zcu.fav.kiv.jsim.core;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import java.time.Instant;

import org.junit.Before;
import org.junit.Test;

import cz.zcu.fav.kiv.jsim.process.SimpleProcess;

/**
 * JSimCalendarEventTest.java
 *    29. 10. 2015
 * @author Lukáš Kvídera
 * @version 0.0
 */
public class JSimCalendarEventTest {

  private JSimCalendarEvent event;

  @Before
  public void setUp() throws Exception {
    this.event = new JSimCalendarEvent(3.31337,
        new SimpleProcess("Test process"));
  }

  @Test
  public void testGetTime() {
    assertEquals("Event time is different!", 3.31337, this.event.getTime().getTime(), 0.0);
  }

//  @Test
//  public void testGetProcess() {
//    assertEquals("Process name is different!", "Test process", this.event.getProcess());
//  }
//
//  @Test
//  public void testGetProcessNumber() {
//    assertEquals("Process id is different!", 0L, this.event.getProcessNumber());
//  }

  @Test
  public void testGetCreationTime() {
    assertTrue("Creation time is older than five miliseconds!",
        this.event.getCreationTime() - Instant.now().toEpochMilli() < 5);
  }

  @Test
  public void testCompareTo() throws Exception {
    final JSimCalendarEvent e1 = new JSimCalendarEvent(32.1,
        new SimpleProcess("t1"));
    final JSimCalendarEvent e2 = new JSimCalendarEvent(32.3,
        new SimpleProcess("t2"));

    assertEquals("Events are not equal!", 0, e1.compareTo(e1));
    assertEquals("Events are in different order!", -1, e1.compareTo(e2));
    assertEquals("Events are in different order!", 1, e2.compareTo(e1));
  }
}
