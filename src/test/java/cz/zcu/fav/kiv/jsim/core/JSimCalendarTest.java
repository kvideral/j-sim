package cz.zcu.fav.kiv.jsim.core;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;

import org.junit.Before;
import org.junit.Test;

import cz.zcu.fav.kiv.jsim.core.exception.JSimInvalidParametersException;
import cz.zcu.fav.kiv.jsim.link.JSimTimeFactory.JSimTime;
import cz.zcu.fav.kiv.jsim.process.SimpleProcess;

/**
 * JSimCalendarTest.java
 *    29. 10. 2015
 * @author Lukáš Kvídera
 * @version 1.0
 */
public class JSimCalendarTest {

  private JSimCalendar calendar;
  private SimpleProcess process;

  @Before
  public void setUp() throws Exception {
    this.calendar = new JSimCalendar();
    this.process = new SimpleProcess("Test process");
  }

  @Test(expected = JSimInvalidParametersException.class)
  public void testAddEntry_TimeOutOfRange_ShouldThrowException() throws Exception {
    this.calendar.addEntry(-1.0 /* out of range */, this.process);
  }

  @Test(expected = NullPointerException.class)
  public void testAddEntry_NullProcess_ShouldThrowException() throws Exception {
    this.calendar.addEntry(-1.0 /* out of range */, null);
  }

  @Test(expected = NullPointerException.class)
  public void testAddWholeEvent_NullProcess_ShouldThrowException() throws Exception {
    this.calendar.addWholeEvent(null);
  }

  @Test
  public void testAddEntry_Insert_OK() throws Exception {
    this.calendar.addEntry(1.0, this.process);
    assertEquals("Unexpected process returned!", this.process, this.calendar.getFirstProcess());

    this.calendar.addEntry(3.0, new SimpleProcess("Another process"));
    assertEquals("Unexpected process returned!", this.process, this.calendar.getFirstProcess());

    this.calendar.addEntry(2.0, new SimpleProcess("Another process"));
    assertEquals("Unexpected process returned!", this.process, this.calendar.getFirstProcess());

    this.calendar.addEntry(1.5, new SimpleProcess("Another process"));
    assertEquals("Unexpected process returned!", this.process, this.calendar.getFirstProcess());
  }

  @Test(expected = JSimInvalidParametersException.class)
  public void testDeleteEntries_NullProcess_ShouldThrowException() {
    this.calendar.deleteEntries(null, true);
  }

  @Test
  public void testDeleteEntries_EmptyEventList_DoNotRemove() {
    final int deletedEntries = this.calendar.deleteEntries(this.process, false);
    assertEquals("Different count of deleted entries!", 0,  deletedEntries);
  }

  @Test
  public void testDeleteEntries_Process_RemoveSingleEvent() {
    this.calendar.addEntry(0.0, this.process);
    final int deletedEntries = this.calendar.deleteEntries(this.process, false);
    assertEquals("Different count of deleted entries!", 1,  deletedEntries);
  }

  @Test
  public void testDeleteEntries_Processes_RemoveSingleEvent() {
    this.calendar.addEntry(0.0, this.process);
    this.calendar.addEntry(0.0, this.process);
    final int deletedEntries = this.calendar.deleteEntries(this.process, false);
    assertEquals("Different count of deleted entries!", 1,  deletedEntries);
  }

  @Test
  public void testDeleteEntries_Processes_RemoveAllEvents() {
    this.calendar.addEntry(0.0, this.process);
    this.calendar.addEntry(0.0, this.process);
    final int deletedEntries = this.calendar.deleteEntries(this.process, true);
    assertEquals("Different count of deleted entries!", 2,  deletedEntries);
  }

  @Test
  public void testGetFirstProcessTime() {
    this.calendar.addEntry(0.31337, this.process);
    this.calendar.addEntry(0.31338, this.process);
    final JSimTime processTime = this.calendar.getFirstProcessTime();
    assertEquals("Different count of deleted entries!", 0.31337,  processTime.getTime(), 0.0);
  }

  @Test
  public void testJump_SingleProcess_EmptyHead() {
    this.calendar.addEntry(0.31337, this.process);
    this.calendar.jump();

    final JSimProcess firstProcess = this.calendar.getFirstProcess();
    assertNull("Process has not been removed!", firstProcess);

    final JSimTime processTime = this.calendar.getFirstProcessTime();
    assertEquals("Different count of deleted entries!", -1.0,  processTime.getTime(), 0.0);
  }

  @Test
  public void testJump_MultipleProcesses_SecondProcess() throws Exception {
    this.calendar.addEntry(0.31337, new SimpleProcess("First process"));
    this.calendar.addEntry(0.31339, new SimpleProcess("Last process"));
    this.calendar.addEntry(0.31338, this.process);
    this.calendar.jump();

    final JSimProcess firstProcess = this.calendar.getFirstProcess();
    assertEquals("Process has not been removed!", this.process, firstProcess);

    final JSimTime processTime = this.calendar.getFirstProcessTime();
    assertEquals("Different count of deleted entries!", 0.31338,  processTime.getTime(), 0.0);
  }

  @Test
  public void testIsEmpty_IsEmpty_True() {
    assertTrue("Calendar is not empty!", this.calendar.isEmpty());
  }

  @Test
  public void testIsEmpty_IsNotEmpty_False() {
    this.calendar.addEntry(0.0, this.process);
    assertFalse("Calendar is empty!", this.calendar.isEmpty());
  }
}
