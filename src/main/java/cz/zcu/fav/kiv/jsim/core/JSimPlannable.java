package cz.zcu.fav.kiv.jsim.core;

import cz.zcu.fav.kiv.jsim.link.JSimTimeFactory.JSimTime;

/**
 * JSimPlannable.java
 *    31. 10. 2015
 * @author Lukáš Kvídera
 * @version 1.0
 */
public interface JSimPlannable {

  /**
   * Ijnection point of global time for single simulation.
   *
   * Process can tell scheduler, when he should plan the process.
   * @param time Global time instance from simulation shared between processes.
   */
  void setTime(JSimTime time);

  /**
   * Adds task time to process, so process no when will be fired all it's tasks.
   * @param time scheduled time of task
   */
  void setSheduledFor(JSimTime time);
}
