/*
 *  Copyright (c) 2000-2006 Jaroslav Kačer <jaroslav@kacer.biz>
 *  Copyright (c) 2003 Pavel Domecký
 *  Copyright (c) 2004 University of West Bohemia, Pilsen, Czech Republic
 *  Licensed under the Academic Free License version 2.1
 *  J-Sim source code can be downloaded from http://www.j-sim.zcu.cz/
 *
 */

package cz.zcu.fav.kiv.jsim.gui;

import java.awt.BorderLayout;
import java.awt.Container;
import java.awt.Font;
import java.awt.GridLayout;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.util.ArrayList;
import java.util.Observable;
import java.util.Observer;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JSplitPane;
import javax.swing.JTextArea;
import javax.swing.JTextField;
import javax.swing.SwingConstants;

import cz.zcu.fav.kiv.jsim.core.JSimSimulation;
import cz.zcu.fav.kiv.jsim.core.JSimSimulationMode;
import cz.zcu.fav.kiv.jsim.core.JSimSimulationState;
import cz.zcu.fav.kiv.jsim.core.exception.JSimInvalidParametersException;
import cz.zcu.fav.kiv.jsim.core.exception.JSimMethodNotSupportedException;
import cz.zcu.fav.kiv.jsim.core.exception.JSimSimulationAlreadyTerminatedException;

/**
 * The JSimMainWindow class provides services for graphic output. You should never use this class directly since the JSimSimulation class
 * contains methods for running in graphic mode.
 *
 * @author Pavel DOMECKÝ
 * @author Jarda KAČER
 *
 * @version J-Sim version 0.6.0
 *
 * @since J-Sim version 0.1.1
 */
public class JSimMainWindow extends JFrame
{
  /**
   * Serialization identification.
   */
  private static final long serialVersionUID = -4221191798348575280L;

  /**
   * Shows the simulation's current time and its changes.
   */
  class JFieldCurrentTime extends JTextField implements Observer
  {
    /**
     * Serialization identification.
     */
    private static final long serialVersionUID = 8207452821306371405L;

    /**
     * Creates a new JTextField having the specified count of columns.
     *
     * @param columns
     *            Number of columns.
     */
    JFieldCurrentTime(final int columns)
    {
      super(columns);
    } // constructor

    /**
     * Rewrites the old time in the text field with the current simulation time.
     */
    @Override
    public void update(final Observable o, final Object arg)
    {
      final double newTime;

//      newTime = JSimMainWindow.this.myParent.getCurrentTime();
//
//      if (newTime != JSimCalendar.NEVER) {
//        JSimMainWindow.this.fieldCurrentTime.setText(Double.toString(newTime));
//      } else {
//        JSimMainWindow.this.fieldCurrentTime.setText("Simulation terminated.");
//      }
    } // update
  } // inner class JFieldCurrentTime

  // ------------------------------------------------------------------------------------------------------------------------------------

  /**
   * An observer responsible for disabling control buttons when the main window is waiting for its closure by the user. Applicable in the
   * GUI batch mode only.
   */
  class CommonChanges implements Observer
  {
    /**
     * Tests whether the window is waiting for manual closure and if it is, disables control buttons used in the GUI batch mode.
     */
    @Override
    public void update(final Observable o, final Object arg)
    {
      if (JSimMainWindow.this.myParent.getWaitingForWindowClosure())
      {
        JSimMainWindow.this.buttonContinue.setEnabled(false);
        JSimMainWindow.this.buttonPause.setEnabled(false);
      } // if
    } // update
  } // inner class CommonChanges

  // ------------------------------------------------------------------------------------------------------------------------------------

  /*
   * The main window's window adapter. Performs some cleanup when the window is closed.
   */
  class MyWindowAdapter extends WindowAdapter
  {
    @Override
    public void windowClosing(final WindowEvent e)
    {
      JSimMainWindow.this.actionQuit();
    } // windowClosing
  } // inner class MyWindowAdapter

  // ------------------------------------------------------------------------------------------------------------------------------------

  /**
   * The main window's action adapter. Performs actions associated with buttons of the main window.
   */
  class MyActionAdapter implements ActionListener
  {
    @Override
    public void actionPerformed(final ActionEvent e)
    {
      final Object source = e.getSource();

      if (source == JSimMainWindow.this.buttonQuit) {
        JSimMainWindow.this.actionQuit();
      }
      if (source == JSimMainWindow.this.buttonOneStep) {
        JSimMainWindow.this.actionRunOneStep();
      }
      if (source == JSimMainWindow.this.buttonUntil) {
        JSimMainWindow.this.actionRunUntilTimeLimit();
      }
      if (source == JSimMainWindow.this.buttonNoOfSteps) {
        JSimMainWindow.this.actionRunNumberOfSteps();
      }
      if (source == JSimMainWindow.this.buttonPause) {
        JSimMainWindow.this.actionPause();
      }
      if (source == JSimMainWindow.this.buttonContinue) {
        JSimMainWindow.this.actionContinue();
      }
    } // actionPerformed
  } // inner class MyActionAdapter

  // ------------------------------------------------------------------------------------------------------------------------------------

  /**
   * The main window's mouse adapter. If the user double clicks on a list, the selected item's detailed info window is opened.
   */
  class MyMouseAdapter extends MouseAdapter
  {
    @Override
    public void mouseClicked(final MouseEvent e)
    {
      if (e.getClickCount() == 2) {
        JSimMainWindow.this.openUpDetailedInfoWindow((JSimMainWindowList) e.getSource());
      }
    } // mouseClicked
  } // inner class MyMouseAdapter

  // ------------------------------------------------------------------------------------------------------------------------------------

  public static final int LIST_TYPE_PROCESS = 1;
  public static final int LIST_TYPE_QUEUE = 2;
  public static final int LIST_TYPE_SEMAPHORE = 3;

  // ------------------------------------------------------------------------------------------------------------------------------------

  /**
   * Common logger for all instances of this class. By default, all logging information goes to a file. Only severe events go to the
   * console, in addition to a file.
   */
  private static final Logger logger;

  /**
   * Simulation this window belongs to.
   */
  private final JSimSimulation myParent;

  /**
   * The simulation mode: text, GUI batch, or GUI interactive.
   */
  private final JSimSimulationMode mode;

  /**
   * The lock used to suspend the main thread while the window is open.
   */
  private final Object graphicLock;

  /**
   * The lock used to notify the main thread while the simulation is paused in GUI batch mode.
   */
  private final Object stepLock;

  /**
   * The lock used to notify the main thread while the simulation waits in waitForWindowClosure().
   */
  private final Object endLock;

  /**
   * The main window's action listener.
   */
  private final MyActionAdapter myActionListener;

  /**
   * The main window's mouse listener.
   */
  private final MyMouseAdapter myMouseListener;

  /**
   * The main window's window listener.
   */
  private final MyWindowAdapter myWindowListener;

  // Panels
  private final JPanel buttonPanel;
  //private JPanel listPanel;
  private final JPanel processesPanel;
  private final JPanel queuesPanel;

  // TextFields
  private JTextField fieldUntil;
  private JTextField fieldNoOfSteps;

  // Observers
  private final JFieldCurrentTime fieldCurrentTime;
  private final CommonChanges commonCh;

  // Buttons
  private final JButton buttonQuit;
  private JButton buttonOneStep;
  private JButton buttonUntil;
  private JButton buttonNoOfSteps;
  private JButton buttonPause;
  private JButton buttonContinue;

  // SplitPanes
  private final JSplitPane splitPane;
  private final JSplitPane splitListPane;

  // ScrollPanes
  private final JScrollPane userOutputScrollPane;
  private final JScrollPane processListScrollPane;
  private final JScrollPane queueListScrollPane;

  // Lists
  private final JSimMainWindowList processList;
  private final JSimMainWindowList queueList;

  // TextArea
  private final JTextArea userOutput;

  /**
   * Flag saying whether it is possible to close the window.
   */
  private boolean canClose;

  /**
   * Flag saying whether the quit button has been pressed.
   */
  private boolean quitPressed;

  /**
   * Flag saying whether the pause button has been pressed.
   */
  private boolean paused;

  /**
   * The main dispatcher of changes generated during simulation progress. Used to notify various GUI components.
   */
  private final JSimChange guiUpdate;

  /**
   * A list of open detailed info windows.
   */
  private final ArrayList<JSimDisplayable> openDetailedWindows;

  /**
   * The new-line property of the OS used.
   */
  private final String newLine;

  // ------------------------------------------------------------------------------------------------------------------------------------

  /**
   * The static block initializes all static attributes.
   */
  static
  {
    logger = Logger.getLogger(JSimMainWindow.class.getName());
  } // static

  // ------------------------------------------------------------------------------------------------------------------------------------

  /**
   * Creates a new main window with several buttons, input fields and a text area. Appearance of the window depends on the simulation's
   * mode.
   *
   * @param simulation
   *            The simulation object that owns this newly created window. It must not be null.
   * @param simMode
   *            The mode in which the simulation runs, design of the window depends on it.
   *
   * @exception JSimSimulationAlreadyTerminatedException
   *                This exception is thrown out if the simulation has already terminated.
   * @exception JSimInvalidParametersException
   *                This exception is thrown out if the simulation is null or one of its locks cannot be obtained.
   */
  public JSimMainWindow(final JSimSimulation simulation, final JSimSimulationMode simMode) throws JSimSimulationAlreadyTerminatedException, JSimInvalidParametersException
  {
    super("J-Sim Main Window");

    if (simulation == null) {
      throw new JSimInvalidParametersException("JSimMainWindow.JSimMainWindow(): simulation");
    }

    if (simulation.getSimulationState() == JSimSimulationState.TERMINATED) {
      throw new JSimSimulationAlreadyTerminatedException("JSimMainWindow.JSimMainWindow()");
    }

    this.myParent = simulation;
    this.mode = simMode;

    this.graphicLock = this.myParent.getGraphicLock();
    if (this.graphicLock == null) {
      throw new JSimInvalidParametersException("JSimMainWindow.JSimMainWindow(): simulation[graphicLock]");
    }

    this.stepLock = this.myParent.getStepLock();
    if (this.stepLock == null) {
      throw new JSimInvalidParametersException("JSimMainWindow.JSimMainWindow(): simulation[stepLock]");
    }

    this.endLock = this.myParent.getWindowClosureLock();
    if (this.endLock == null) {
      throw new JSimInvalidParametersException("JSimMainWindow.JSimMainWindow(): simulation[endLock]");
    }

    this.canClose = true;
    this.quitPressed = false;
    this.paused = false;

    this.guiUpdate = this.myParent.getChange();
    this.commonCh = new CommonChanges();
    this.openDetailedWindows = new ArrayList<>();
    this.newLine = System.getProperty("line.separator");

    // Initializations for both graphic modes
    this.myActionListener = new MyActionAdapter();
    this.myMouseListener = new MyMouseAdapter();
    this.myWindowListener = new MyWindowAdapter();
    this.addWindowListener(this.myWindowListener);

    this.setFont(new Font("SansSerif", Font.PLAIN, 12));

    this.buttonPanel = new JPanel();
    this.buttonQuit = new JButton("Quit");
    this.fieldCurrentTime = new JFieldCurrentTime(10);
    this.fieldCurrentTime.setText(Double.toString(this.myParent.getCurrentTime()));
    this.fieldCurrentTime.setEditable(false);
    this.guiUpdate.addObserver(this.fieldCurrentTime);

    this.userOutput = new JTextArea(25, 58);
    this.userOutput.setFont(new Font("MonoSpaced", Font.PLAIN, 12));
    this.userOutputScrollPane = new JScrollPane(this.userOutput);

    // Process list and its scrollpane
    this.processList = new JSimMainWindowList(this.myParent, LIST_TYPE_PROCESS);
    this.guiUpdate.addObserver(this.processList);
    this.processList.addMouseListener(this.myMouseListener);
    this.processListScrollPane = new JScrollPane(this.processList);

    // Queue list and its scrollpane
    this.queueList = new JSimMainWindowList(this.myParent, LIST_TYPE_QUEUE);
    this.guiUpdate.addObserver(this.queueList);
    this.queueList.addMouseListener(this.myMouseListener);
    this.queueListScrollPane = new JScrollPane(this.queueList);

    // Processes panel = label + processListScrollPane
    this.processesPanel = new JPanel();
    this.processesPanel.setLayout(new BoxLayout(this.processesPanel, BoxLayout.Y_AXIS));
    this.processesPanel.add(new JLabel("Processes:", SwingConstants.CENTER));
    this.processesPanel.add(this.processListScrollPane);

    // Queues panel = label + queueListScrollPane
    this.queuesPanel = new JPanel();
    this.queuesPanel.setLayout(new BoxLayout(this.queuesPanel, BoxLayout.Y_AXIS));
    this.queuesPanel.add(new JLabel("Queues:", SwingConstants.CENTER));
    this.queuesPanel.add(this.queueListScrollPane);

    // splitListPane divides processesPanel and queuesPanel
    this.splitListPane = new JSplitPane(JSplitPane.VERTICAL_SPLIT, this.processesPanel, this.queuesPanel);
    this.splitListPane.setDividerLocation(210);
    this.splitListPane.setOneTouchExpandable(true);

    // splitPane divides userOutputScrollPane and splitListPane
    this.splitPane = new JSplitPane(JSplitPane.HORIZONTAL_SPLIT, this.userOutputScrollPane, this.splitListPane);
    this.splitPane.setDividerLocation(0.8);
    this.splitPane.setOneTouchExpandable(true);

    // Initializations specific for each simulation mode
    switch (this.mode)
    {
      case GUI_BATCH:
        this.buttonPanel.setLayout(new GridLayout(1, 5));

        this.buttonPause = new JButton("Pause");
        this.buttonPause.setToolTipText("Suspends the simulation");

        this.buttonContinue = new JButton("Continue");
        this.buttonContinue.setEnabled(false);
        this.buttonContinue.setToolTipText("Continues the simulation");

        this.buttonPanel.add(new JLabel("Current Time:"));
        this.buttonPanel.add(this.fieldCurrentTime);

        this.buttonPanel.add(this.buttonPause);
        this.buttonPause.addActionListener(this.myActionListener);

        this.buttonPanel.add(this.buttonContinue);
        this.buttonContinue.addActionListener(this.myActionListener);

        this.buttonPanel.add(this.buttonQuit);
        this.buttonQuit.addActionListener(this.myActionListener);

        this.guiUpdate.addObserver(this.commonCh);
        this.paused = false;
        break;

      case GUI_INTERACTIVE:
        this.buttonPanel.setLayout(new GridLayout(2, 6));
        this.buttonOneStep = new JButton("Run one step");
        this.buttonUntil = new JButton("Run until the time limit");
        this.buttonNoOfSteps = new JButton("Run N steps");

        this.fieldUntil = new JTextField(10);
        this.fieldUntil.setText(Double.toString(this.myParent.getCurrentTime()));
        this.fieldNoOfSteps = new JTextField(5);
        this.fieldNoOfSteps.setText("5");

        this.buttonPanel.add(new JLabel("Current Time:", SwingConstants.CENTER));
        this.buttonPanel.add(new JLabel("Time Limit:", SwingConstants.RIGHT));

        this.buttonPanel.add(this.fieldUntil);
        this.buttonPanel.add(this.buttonUntil);
        this.buttonUntil.addActionListener(this.myActionListener);
        this.buttonPanel.add(this.buttonQuit);
        this.buttonQuit.addActionListener(this.myActionListener);
        this.buttonPanel.add(this.fieldCurrentTime);
        this.buttonPanel.add(new JLabel("Number of steps to run:", SwingConstants.RIGHT));
        this.buttonPanel.add(this.fieldNoOfSteps);
        this.buttonPanel.add(this.buttonNoOfSteps);
        this.buttonNoOfSteps.addActionListener(this.myActionListener);
        this.buttonPanel.add(this.buttonOneStep);
        this.buttonOneStep.addActionListener(this.myActionListener);
        break;

      default:
        logger.log(Level.WARNING, "Main GUI window: Unknown GUI mode specified!");
        break;
    } // switch

    final Container contentPane = this.getContentPane();
    contentPane.setLayout(new BorderLayout());
    contentPane.add(this.buttonPanel, BorderLayout.NORTH);
    contentPane.add(this.splitPane, BorderLayout.CENTER);

    this.setTitle("J-Sim: " + this.myParent.getSimulationName());
    this.pack();
    this.setLocationRelativeTo(null);
  } // constructor

  // ------------------------------------------------------------------------------------------------------------------------------------

  /**
   * Returns the simulation object to which this window belongs to.
   *
   * @return The simulation object to which this window belongs to.
   */
  public JSimSimulation getSimulation()
  {
    return this.myParent;
  } // getSimulation

  // ------------------------------------------------------------------------------------------------------------------------------------

  /**
   * Closes the window, notifies the main thread and returns control to the simulation. Called when the `Quit' button is pressed.
   */
  private synchronized void actionQuit()
  {
    switch (this.mode)
    {
      case GUI_BATCH:
        this.myParent.windowIsClosing(this);
        logger.log(Level.FINE, "The main simulation window is closing up.");
        this.quitPressed = true;
        // If we are waiting in pause we must let step() exit first. The step will not be executed
        // because step() detects that the button has been pressed after it is woken up.
        if (this.paused)
        {
          this.paused = false;
          synchronized (this.stepLock)
          {
            this.stepLock.notify();
          } // synchronized (stepLock)
        } // if paused

        // Destroy the window
        this.setVisible(false);
        this.dispose();
        logger.log(Level.FINE, "Main window destroyed.");

        // If we are waiting in waitForWindowClosure() we must let it exit.
        if (this.myParent.getWaitingForWindowClosure())
        {
          synchronized (this.endLock)
          {
            this.endLock.notify();
          } // synchronized (endLock)
        } // if waiting for closure
        break;

      case GUI_INTERACTIVE:
        synchronized (this.graphicLock)
        {
          // If the simulation is running we cannot close the window.
          if (!this.canClose) {
            JOptionPane.showMessageDialog(null, "Please wait until the currently running operation finishes and then try again.", "Closing the window", JOptionPane.ERROR_MESSAGE);
          } else
          {
            this.myParent.windowIsClosing(this);
            logger.log(Level.FINE, "The main simulation window is closing up.");
            this.graphicLock.notify();
            this.setVisible(false);
          } // else cannot close
        } // synchronized (graphicLock)
        break;

      default:
        logger.log(Level.WARNING, "Main GUI window: Unknown GUI mode specified! Cleanup not done properly.");
        break;
    } // switch mode
  } // actionQuit

  /**
   * Runs one simulation step. Called when the `Run one step' button is pressed.
   */
  private void actionRunOneStep()
  {
    boolean result;

    // We needn't check canClose here because this button is disabled when an action is being performed.
    this.canClose = false;
    result = true;
    this.buttonQuit.setEnabled(false);
    this.disableRunButtons();

    // Let's do one step now
    try
    {
      result = this.myParent.step();
    } // try
    catch (final JSimMethodNotSupportedException e) // This should never happen.
    {
      logger.log(Level.WARNING, "The simulation refused to execute a step.", e);
    } // catch

    // If the simulation has already terminated, we must inform the user and let buttons disabled.
    if (result == false) {
      JOptionPane.showMessageDialog(null, "The simulation has terminated.", "J-Sim", JOptionPane.INFORMATION_MESSAGE);
    } else {
      this.enableRunButtons();
    }

    // Always enable the quit button
    this.buttonQuit.setEnabled(true);
    this.canClose = true;
  } // actionRunOneStep

  /**
   * Runs an unspecified number of simulation steps until the simulation time is equal to or greater than the time specified in the `Time
   * Limit' input field. Called when the `Run until the time limit' button is pressed.
   */
  private void actionRunUntilTimeLimit()
  {
    boolean result;
    double endTime = 0.0;
    boolean inputOK = true;

    // Reading the number is a potentially dangerous operation.
    try
    {
      endTime = Double.parseDouble(this.fieldUntil.getText());
    }
    catch (final NumberFormatException e)
    {
      inputOK = false;
    }

    if (endTime <= this.myParent.getCurrentTime()) {
      inputOK = false;
    }

    if (inputOK)
    {
      // We needn't check canClose here because this button is disabled when an action is being performed.
      this.canClose = false;
      this.buttonQuit.setEnabled(false);
      this.disableRunButtons();

      // Now we are doing as steps until we reach the desired time (or get over it).
      // But if the simulation terminates we break up the cycle, of course.
      result = true;
      try
      {
        while ((this.myParent.getCurrentTime() < endTime) && (result == true)) {
          result = this.myParent.step();
        }
      } // try
      catch (final JSimMethodNotSupportedException e) // This should never happen.
      {
        logger.log(Level.WARNING, "The simulation refused to execute a step.", e);
      } // catch

      // If the simulation has already terminated, we must inform the user and let buttons disabled.
      if (result == false) {
        JOptionPane.showMessageDialog(null, "The simulation has terminated.", "J-Sim", JOptionPane.INFORMATION_MESSAGE);
      } else {
        this.enableRunButtons();
      }

      // Always enable the quit button
      this.buttonQuit.setEnabled(true);
      this.canClose = true;
    } // input OK
    else {
      JOptionPane.showMessageDialog(null, "The value entered is invalid or less than the current time. Please correct and try again.", "J-Sim", JOptionPane.ERROR_MESSAGE);
    }
  } // actionRunUntilTimeLimit

  /**
   * Runs the number of steps specified in the `Number of steps to run' input field. Called when the `Run N steps' button is pressed.
   */
  private void actionRunNumberOfSteps()
  {
    boolean result;
    int howManySteps = 0;
    boolean inputOK = true;
    int i;

    // Reading the number is a potentially dangerous operation.
    try
    {
      howManySteps = Integer.parseInt(this.fieldNoOfSteps.getText());
    } // try
    catch (final NumberFormatException e)
    {
      inputOK = false;
    } // catch

    if (howManySteps < 1) {
      inputOK = false;
    }

    if (inputOK)
    {
      // We needn't check canClose here because this button is disabled when an action is being performed.
      this.canClose = false;
      this.buttonQuit.setEnabled(false);
      this.disableRunButtons();

      // Now we are doing as many steps as was required.
      // But if the simulation terminates we break up the cycle, of course.
      result = true;

      try
      {
        for (i = 0; (i < howManySteps) && (result == true); i++) {
          result = this.myParent.step();
        }
      } // try
      catch (final JSimMethodNotSupportedException e) // This should never happen.
      {
        logger.log(Level.WARNING, "The simulation refused to execute a step.", e);
      } // catch

      // If the simulation has already terminated, we must inform the user and let buttons disabled.
      if (result == false) {
        JOptionPane.showMessageDialog(null, "The simulation has terminated.", "J-Sim", JOptionPane.INFORMATION_MESSAGE);
      } else {
        this.enableRunButtons();
      }

      // Always enable the quit button.
      this.buttonQuit.setEnabled(true);
      this.canClose = true;
    } // input OK
    else {
      JOptionPane.showMessageDialog(null, "The value entered is invalid. Please correct and try again.", "J-Sim", JOptionPane.ERROR_MESSAGE);
    }
  } // actionRunNumberOfSteps

  /**
   * Suspends the simulation. Called when the `Pause' button is pressed. The step() method will blocked after this method is invoked.
   */
  private void actionPause()
  {
    this.paused = true;
    this.buttonPause.setEnabled(false);
    this.buttonContinue.setEnabled(true);
  } // actionPause

  /**
   * Unblocks the simulation. Called when the `Continue' button is pressed. The step() method will be unblocked and it will continue.
   */
  private void actionContinue()
  {
    this.paused = false;
    this.buttonPause.setEnabled(true);
    this.buttonContinue.setEnabled(false);

    synchronized (this.stepLock)
    {
      this.stepLock.notify();
    } // synchronized(stepLock)
  } // actionContinue

  // ------------------------------------------------------------------------------------------------------------------------------------

  /**
   * Returns true if the 'Quit' button has been pressed.
   *
   * @return True if the 'Quit' button has been pressed, false otherwise.
   */
  public synchronized final boolean getQuitPressed()
  {
    return this.quitPressed;
  } // getQuitPressed

  /**
   * Returns true if the 'Pause' button has been pressed.
   *
   * @return True if the 'Pause' button has been pressed, false otherwise.
   */
  public synchronized final boolean getPausePressed()
  {
    return this.paused;
  } // getPausePressed

  // ------------------------------------------------------------------------------------------------------------------------------------

  /**
   * Prints out a string to the user text area at the bottom of the window.
   *
   * @param s
   *            The string to be printed out.
   * @param code
   *            A code saying whether it is an info message (PRN_MESSAGE) or an error message (PRN_ERROR).
   * @param appendNewLine
   *            A flag indicating that the new line character should be appended to the text.
   */
  public void printString(final String s, final int code, final boolean appendNewLine)
  {
    if (appendNewLine) {
      this.userOutput.append(s + this.newLine);
    } else {
      this.userOutput.append(s);
    }

    // Set the caret behind the last char, supplyies scrolling of the userOutput
    this.userOutput.setCaretPosition(this.userOutput.getText().length());

    if (code == JSimSimulation.PRN_ERROR) {
      Toolkit.getDefaultToolkit().beep();
    }
  } // printString

  // ------------------------------------------------------------------------------------------------------------------------------------

  /**
   * Creates and displays a detailed info window that displays detailed characteristics of the object corresponding to the selected item
   * from a list placed inside the main window.
   *
   * @param list
   *            The list from which the item is selected.
   */
  private void openUpDetailedInfoWindow(final JSimMainWindowList list)
  {
    int selectedIndex = -1;
    JSimDisplayable jsd;
    JDialog detailedWindow;

    try
    {
      if (!list.isSelectionEmpty())
      {
        selectedIndex = list.getSelectedIndex();
        jsd = list.getInfoTable().get(selectedIndex);
        if (jsd != null)
        {
          if (!this.openDetailedWindows.contains(jsd))
          {
            this.openDetailedWindows.add(jsd);
            detailedWindow = jsd.createDetailedInfoWindow(this);
            this.guiUpdate.addObserver((Observer) detailedWindow);
            detailedWindow.setVisible(true);
          } // if window not open yet
        }
      } // if selection not empty
    } // try
    catch (final IndexOutOfBoundsException e)
    {
      logger.log(Level.WARNING, "Unable to open a detailed info window for list index " + selectedIndex, e);
    } // catch
  } // openUpDetailedInfoWindow

  /**
   * When a detailed info window shuts up, it should invoke its main window's removeDisplayableFromInfoWindows() method in order to inform
   * it that its displayable object is no longer showing. This will allow the main window to open a new detailed info window in the future
   * if the user requests it.
   *
   * @param jsd
   *            The JSimDisplayable object whose detailed info window is just shutting up.
   */
  public void removeDisplayableFromOpenInfoWindows(final JSimDisplayable jsd)
  {
    this.openDetailedWindows.remove(jsd);
  } // closeDetailedInfoWindow

  // ------------------------------------------------------------------------------------------------------------------------------------

  /**
   * Disables all buttons except of the `Quit' button.
   */
  private void disableRunButtons()
  {
    this.buttonUntil.setEnabled(false);
    this.buttonOneStep.setEnabled(false);
    this.buttonNoOfSteps.setEnabled(false);
  } // disableRunButtons

  /**
   * Enables all buttons except of the `Quit' button.
   */
  private void enableRunButtons()
  {
    this.buttonUntil.setEnabled(true);
    this.buttonOneStep.setEnabled(true);
    this.buttonNoOfSteps.setEnabled(true);
  } // enableRunButtons

} // class JSimMainWindow
