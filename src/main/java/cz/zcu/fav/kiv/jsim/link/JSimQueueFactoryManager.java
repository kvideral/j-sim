package cz.zcu.fav.kiv.jsim.link;

import java.util.UUID;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentMap;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import cz.zcu.fav.kiv.jsim.link.JSimTimeFactory.JSimTime;

/**
 * JSimQueueFactoryManager.java
 *    6. 11. 2015
 * @author Lukáš Kvídera
 * @version 0.0
 */
public class JSimQueueFactoryManager {

  private static final Logger LOG = LoggerFactory.getLogger(JSimQueueFactoryManager.class);
  private static ConcurrentMap<UUID, JSimQueueFactory> factoryCache = new ConcurrentHashMap<>();

  public static JSimQueueFactory createNewFactory(final UUID simulationId, final JSimTime globalSimulationTime) {
    LOG.trace("createNewFactory(simulationId={}, globalSimulationTime={})", simulationId, globalSimulationTime);
    return factoryCache.computeIfAbsent(simulationId, key -> new JSimQueueFactory(globalSimulationTime));
  }

  /**
   * @return
   *
   */
  public static JSimQueueFactory getInstance(final UUID simulationId) {
    LOG.trace("getInstance(simulationId={})", simulationId);
    return factoryCache.get(simulationId);
  }

}
