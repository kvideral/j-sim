/*
 *  Copyright (c) 2000-2006 Jaroslav Kačer <jaroslav@kacer.biz>
 *  Copyright (c) 2004 University of West Bohemia, Pilsen, Czech Republic
 *  Licensed under the Academic Free License version 2.1
 *  J-Sim source code can be downloaded from http://www.j-sim.zcu.cz/
 *
 */

package cz.zcu.fav.kiv.jsim.core;

import java.util.Iterator;
import java.util.PriorityQueue;
import java.util.Queue;

import cz.zcu.fav.kiv.jsim.core.exception.JSimInvalidParametersException;
import cz.zcu.fav.kiv.jsim.link.JSimTimeFactory.JSimTime;

/**
 * The JSimCalendar class holds information about all scheduled events within a J-Sim simulation. It contains a list of JSimCalendarEvent
 * elements, ordered by their simulation time. J-Sim simulations use this class to schedule processes and to get information about a process
 * which should be run during the next simulation step. You should never need to create an instance of this class.
 *
 * @author Jarda KAČER
 *
 * @version J-Sim version 0.6.0
 *
 * @since J-Sim version 0.0.1
 */
public class JSimCalendar
{
  /**
   * Constant signalling that there is no process scheduled.
   */
  public static final JSimProcessImpl NOBODY = null;

  /**
   * Constant signalling that there is no process scheduled.
   */
  public static final JSimTime NEVER = JSimTime.valueOf(-1.0);

  // ------------------------------------------------------------------------------------------------------------------------------------

  /**
   * This list holds all events of the calendar. Events are always kept ordered, because the list is sorted after every insertion.
   */
  private final Queue<JSimCalendarEvent> eventList = new PriorityQueue<>();

  // ------------------------------------------------------------------------------------------------------------------------------------

  /**
   * Adds a new event with the specified time and the specified process to the calendar.
   *
   * @param absTime The absolute simulation time of the event.
   * @param process The process to be later activated.
   *
   * @exception {@link JSimInvalidParametersException}
   *            This exception is thrown out if the absolute simulation time is negative
   *            or the process is not specified (null).
   */
  public synchronized void addEntry(final double absTime, final JSimProcess process)
  {
    this.addWholeEvent(new JSimCalendarEvent(absTime, process));
  } // addEntry

  /**
   * Adds the specified event to the calendar. This is useful when you already have an event (from outside) and want to add it to the
   * calendar.
   *
   * @param calendarEvent The event to be added to the calendar.
   *
   * @exception JSimInvalidParametersException
   *            This exception is thrown out if the absolute simulation time is negative or
   *            the process is not specified (null).
   */
  public synchronized void addWholeEvent(final JSimCalendarEvent calendarEvent)
  {
    if (!this.eventList.isEmpty() && calendarEvent.getTime().isBefore(this.eventList.peek().getTime())) {
      throw new JSimInvalidParametersException("JSimCalendar.addWholeEvent(): calendarEvent.pastTime");
    }

    if (calendarEvent.getTime().getTime() < 0.0) {
      throw new JSimInvalidParametersException("JSimCalendar.addWholeEvent(): calendarEvent.absTime");
    }
    if (calendarEvent.getProcess() == null) {
      throw new JSimInvalidParametersException("JSimCalendar.addWholeEvent(): calendarEvent.process");
    }

    this.eventList.add(calendarEvent);
  } // addWholeEvent

  /**
   * Deletes one or all events of a process from the calendar. The rest of J-Sim assures that a process will not have more than one event
   * in the calendar so the second atribute is more or less useless.
   *
   * @param process The process whose event(s) are to be deleted.
   * @param all A flag saying that not only one but all events of the given process should be deleted.
   *
   * @return The number of events deleted.
   *
   * @exception JSimInvalidParametersException
   *                This exception is thrown out if the process is not specified (null).
   */
  public synchronized int deleteEntries(final JSimProcess process, final boolean all)
  {
    JSimCalendarEvent entry;
    Iterator<JSimCalendarEvent> it;
    int noOfDeleted = 0;

    if (process == null) {
      throw new JSimInvalidParametersException("JSimCalendar.deleteEntries(): process");
    }

    it = this.eventList.iterator();
    while (it.hasNext() && ((all == true) || (noOfDeleted < 1)))
    {
      entry = it.next();
      if (entry.getProcess() == process)
      {
        it.remove();
        noOfDeleted++;
      } // if
    } // while

    return noOfDeleted;
  } // deleteEntries

  /**
   * Returns the first scheduled process. Its calendar event is at the head of the event list because it is always sorted.
   *
   * @return The first scheduled process or NOBODY if the calendar is empty.
   */
  public synchronized JSimProcess getFirstProcess()
  {
    if (this.eventList.isEmpty()) {
      return NOBODY;
    }

    return this.eventList.peek().getProcess();
  } // getFirstProcess

  /**
   * Returns the time of the event being at the head of the calendar.
   *
   * @return The time of the event being at the head of the calendar or NEVER if the calendar is empty.
   */
  public synchronized JSimTime getFirstProcessTime()
  {
    if (this.eventList.isEmpty()) {
      return NEVER;
    }

    return this.eventList.peek().getTime();
  } // getFirstProcessTime

  /**
   * Deletes the event at the head of the calendar and sets the head to the event which follows the current head.
   */
  public synchronized void jump()
  {
    if (!this.eventList.isEmpty()) {
      this.eventList.remove();
    }
  } // jump

  /**
   * Says whether the calendar is empty. The calendar is empty if it contains no events.
   *
   * @return True if it is empty, false otherwise.
   */
  public synchronized boolean isEmpty()
  {
    return this.eventList.isEmpty();
  } // isEmpty

  /**
   * Returns a string representation of the calendar.
   *
   * @return A string representation of the calendar.
   */
  @Override
  public String toString()
  {
    return this.eventList.toString();
  } // toString

} // class JSimCalendar

