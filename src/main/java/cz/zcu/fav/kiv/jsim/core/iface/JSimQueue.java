package cz.zcu.fav.kiv.jsim.core.iface;

import java.util.function.Consumer;
import java.util.function.Supplier;

import cz.zcu.fav.kiv.jsim.link.JSimObject;

public interface JSimQueue extends Consumer<JSimObject>, Supplier<JSimObject> {

  /**
   * Returns the mean length of the queue. It makes no sense to calculate this value for a queue that has just been created. If no
   * simulation time has passed since its creation NaN (not-a-number) will be returned.
   *
   * @return The mean length of the queue or NaN.
   */
  double getLw();

  /**
   * Returns the mean waiting time spent in the queue by all links already removed from the queue. It makes no sense to calculate this
   * value for a queue without any elements revoved from it. In such a case, NaN (not-a-number) will be returned.
   *
   * @return The mean waiting time spent in the queue by all links already removed from the queue or NaN.
   */
  double getTw();
}
