package cz.zcu.fav.kiv.jsim.link;

/**
 * JSimStatistics.java
 *    11. 12. 2015
 * @author Lukáš Kvídera
 * @version 1.0
 * @since 1.0
 */
public class JSimStatistics {
  private double mean;
  private double variance;
  private long countOfSamples;

  public void accept(final double value) {
    final double delta = value - this.mean;
    this.mean += delta / ++this.countOfSamples;
    this.variance += delta * (value - this.mean);
  }

  /**
   * @return the mean
   */
  public double getMean() {
    return this.mean;
  }

  /**
   * @return the variance
   */
  public double getVariance() {
    return this.variance / this.countOfSamples;
  }

  /**
   * @return the countOfSamples
   */
  public long getCountOfSamples() {
    return this.countOfSamples;
  }

  @Override
  public String toString() {
    return "JSimStatistics [mean=" + this.getMean() + ", variance=" + this.getVariance() + ", countOfSamples="
        + this.countOfSamples + "]";
  }
}
