package cz.zcu.fav.kiv.jsim.process;

import cz.zcu.fav.kiv.jsim.core.JSimProcessImpl;
import cz.zcu.fav.kiv.jsim.link.JSimObject;
import cz.zcu.fav.kiv.jsim.random.JSimConstantDistribution;

/**
 * SimpleProcess.java
 *    12. 12. 2015
 * @author Lukáš Kvídera
 * @version 0.0
 */
public class SimpleProcess extends JSimProcessImpl {

  /**
   * @param name
   * @param distribution
   */
  public SimpleProcess(final String name) {
    super(name, new JSimConstantDistribution(1.0));
  }

  @Override
  protected JSimObject serve() {
    // TODO Auto-generated method stub
    return null;
  }

  @Override
  protected void doWork(final JSimObject o) {

  }

}
