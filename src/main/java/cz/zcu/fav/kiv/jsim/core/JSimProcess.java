package cz.zcu.fav.kiv.jsim.core;

import java.util.Optional;
import java.util.UUID;
import java.util.concurrent.Callable;

import cz.zcu.fav.kiv.jsim.core.exception.JSimInvalidProcessStateException;
import cz.zcu.fav.kiv.jsim.core.iface.JSimPlannable;
import cz.zcu.fav.kiv.jsim.link.JSimObject;

/**
 * JSimTask.java
 *    30. 10. 2015
 * @author Lukáš Kvídera
 * @version 1.0
 */
public interface JSimProcess extends Callable<JSimObject>, JSimPlannable, JSimConversationable {

  /**
   * This method provides scheduler, that creates events for {@link JSimCalendar}
   * @return Scheduler for {@link JSimProcessImpl}
   */
  Optional<JSimScheduler> getScheduler();

  /**
   * Returns state of process.
   * @return the state
   */
  JSimProcessState getProcessState();

  /**
   * Sets the new state
   * @param newState state to set
   * @throws JSimInvalidProcessStateException if process is in state, that does not allow transition
   * to the neWstate
   */
  void setProcessState(final JSimProcessState newState) throws JSimInvalidProcessStateException;

  void setSimulationId(UUID simulationId);

}
