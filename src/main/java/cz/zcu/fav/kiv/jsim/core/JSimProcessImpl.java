/*
 *  Copyright (c) 2000-2006 Jaroslav Kačer <jaroslav@kacer.biz>
 *  Copyright (c) 2003 Pavel Domecký
 *  Copyright (c) 2004 University of West Bohemia, Pilsen, Czech Republic
 *  Licensed under the Academic Free License version 2.1
 *  J-Sim source code can be downloaded from http://www.j-sim.zcu.cz/
 *
 */

package cz.zcu.fav.kiv.jsim.core;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Objects;
import java.util.Optional;
import java.util.Set;
import java.util.UUID;
import java.util.concurrent.atomic.AtomicInteger;

import javax.swing.JDialog;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import cz.zcu.fav.kiv.jsim.core.exception.JSimInvalidParametersException;
import cz.zcu.fav.kiv.jsim.core.exception.JSimInvalidProcessStateException;
import cz.zcu.fav.kiv.jsim.core.exception.JSimKernelPanicException;
import cz.zcu.fav.kiv.jsim.core.exception.JSimSecurityException;
import cz.zcu.fav.kiv.jsim.core.exception.JSimSimulationAlreadyTerminatedException;
import cz.zcu.fav.kiv.jsim.core.exception.JSimTooManyProcessesException;
import cz.zcu.fav.kiv.jsim.gui.JSimDetailedInfoWindow;
import cz.zcu.fav.kiv.jsim.gui.JSimDisplayable;
import cz.zcu.fav.kiv.jsim.gui.JSimMainWindow;
import cz.zcu.fav.kiv.jsim.gui.JSimPair;
import cz.zcu.fav.kiv.jsim.ipc.JSimMessage;
import cz.zcu.fav.kiv.jsim.ipc.JSimMessageBox;
import cz.zcu.fav.kiv.jsim.ipc.JSimMessageForReceiver;
import cz.zcu.fav.kiv.jsim.ipc.JSimSemaphore;
import cz.zcu.fav.kiv.jsim.ipc.JSimSymmetricMessage;
import cz.zcu.fav.kiv.jsim.link.JSimObject;
import cz.zcu.fav.kiv.jsim.link.JSimStatistics;
import cz.zcu.fav.kiv.jsim.link.JSimTimeFactory.JSimTime;
import cz.zcu.fav.kiv.jsim.random.JSimDistribution;

/**
 * A JSimProcess is an independent unit of code, having its own data. Its code is placed in the life() method. Processes have possibilities
 * to schedule themselves as well as other processes, they can also cancel another process's schedule. A process must belong to a
 * simulation. During a simulation execution, processes are interleaved in a pseudo-parallel manner, i.e. just one process is running at the
 * same time. When the currently running process invokes hold(), passivate(), or reactivate(), it is suspended and another process can be
 * executed instead.
 *
 * @author Jarda KAČER
 * @author Pavel DOMECKÝ
 *
 * @version J-Sim version 0.6.0
 *
 * @since J-Sim version 0.0.1
 */
public abstract class JSimProcessImpl implements JSimDisplayable, JSimProcess, Comparable<JSimProcessImpl>
{
  /**
   * Methods passivate(), hold(), and reactivate() will ignore first INT_REQUESTS_TO_IGNORE requests to warn the programmer.
   */
  public static final int INT_REQUESTS_TO_IGNORE  = 1;

  // ------------------------------------------------------------------------------------------------------------------------------------

  /**
   * Common logger for all instances of this class. By default, all logging information goes to a file. Only severe events go to the
   * console, in addition to a file.
   */
  private static final Logger LOG = LoggerFactory.getLogger(JSimProcessImpl.class);

  private static AtomicInteger idCounter = new AtomicInteger(0);

  /**
   * The process's number. It is unique for all process within the same simulation.
   */
  private final long myNumber;

  /**
   * The process's name.
   */
  private final String name;

  /**
   * This process's state &ndash; new, passive, scheduled, active, blocked on semaphore, terminated, etc.
   */
  private JSimProcessState myState;

  /**
   * Flag saying whether this process has been started. Even a new thread can be scheduled, we must distinguish between being new and
   * being started.
   */
//  private boolean haveIBeenStarted;

  /**
   * The simulation time that the process is scheduled for (if it is scheduled).
   */
  private final Set<JSimTime> scheduleTime = new HashSet<>();

  /**
   * The semaphore that this process is currently blocked on, if it is blocked at all.
   */
  private JSimSemaphore semaphoreIAmCurrentlyBlockedOn;

  /**
   * Every process has its own message box where messages sent directly between processes are stored.
   */
  private final JSimMessageBox myMessageBox;

  /**
   * The message clipboard is a single message passed from a process to this process
   * when this process has invoked a blocking receive
   * operation and got blocked inside it.
   */
  private JSimMessage messageClipboard;

  /**
   * A process that this process is waiting for.
   * This process will stay suspended inside a blocking receive operation until the sender
   * sends it a message.
   */
  private final JSimProcessImpl messageSenderIAmWaitingFor;

  private JSimTime simulationTime;

  private UUID simulationId;

  protected final JSimDistribution distribution;

  private final JSimStatistics responseTimeStatistics = new JSimStatistics();

  private double totalProcessingTime;
  protected double processingTime;

  private class Scheduler implements JSimScheduler {

    /**
     *
     */
    public Scheduler() {
      // TODO Auto-generated constructor stub
    }

    /* (non-Javadoc)
     * @see cz.zcu.fav.kiv.jsim.core.JSimScheduler#schedule()
     */
    @Override
    public List<JSimCalendarEvent> getSchedule() {
      return Arrays.asList(
          new JSimCalendarEvent(
              JSimProcessImpl.this.getSimulationTime().getTime() + JSimProcessImpl.this.processingTime,
              JSimProcessImpl.this
              )
          );
    }

    /* (non-Javadoc)
     * @see cz.zcu.fav.kiv.jsim.core.JSimScheduler#getJobsToUnschedule()
     */
    @Override
    public List<JSimCalendarEvent> getJobsToUnschedule() {
      return Collections.emptyList();
    }

  }
  // ------------------------------------------------------------------------------------------------------------------------------------

  /**
   * Creates a new process having a name and belonging to a simulation. If the simulation is already terminated or there is no free number
   * the process is not created and an exception is thrown out. All processes must have their parent simulation object specified. If not,
   * an exception is thrown, too. After the creation, the process is not scheduled and therefore will not run unless explicitely activated
   * by another process or the main program using activate().
   *
   * @param name
   *            Name of the process being created.
   * @param parent
   *            Parent simulation.
   *
   * @exception JSimSimulationAlreadyTerminatedException
   *                This exception is thrown out if the simulation has already been terminated.
   * @exception JSimInvalidParametersException
   *                This exception is thrown out if no parent was specified.
   * @exception JSimTooManyProcessesException
   *                This exception is thrown out if no other process can be added to the simulation specified.
   * @exception JSimKernelPanicException
   *                This exception is thrown out if the simulation is in a unknown state. Do NOT catch this exception!
   */
  public JSimProcessImpl(final String name, final JSimDistribution distribution) {
    this.distribution = distribution;
    this.myNumber = idCounter.incrementAndGet();
    this.name = name;
    this.myState = JSimProcessState.NEW;
    this.semaphoreIAmCurrentlyBlockedOn = null;
    this.myMessageBox = new JSimMessageBox("Message Box for " + name);
    this.messageClipboard = null;
    this.messageSenderIAmWaitingFor = null;
  } // constructor

// ------------------------------------------------------------------------------------------------------------------------------------

  /**
   * @return
   */
  public String getName() {
    return this.name;
  }

  /**
   * This method should never be overwritten or called directly. It call getReady(), life() and finish() and handles some extraordinary
   * situations. You should never use or modify this method (it is final).
   *
   * Should be PRIVATE, but can't be &ndash; the parent's rights are public.
   */
  @Override
  public final JSimObject call() {
    LOG.trace("call()");
    final JSimObject jSimObject = this.serve();
    //TODO notufyx when empty queue
    this.processingTime = this.distribution.next();
    if (jSimObject != null) {
      this.doWork(jSimObject);
      this.totalProcessingTime += this.processingTime;
      this.responseTimeStatistics.accept(
          this.getSimulationTime().getTime() - jSimObject.getEnterTime().getTime()
          + this.processingTime
          );
    } else {
      LOG.info("Nothing to do!");
    }

    return jSimObject;
  } // run

  protected abstract JSimObject serve();
  protected abstract void doWork(JSimObject o);

  protected void updateReaponseTimeStatistics(final double totalTime) {
    this.responseTimeStatistics.accept(totalTime);
  }

  /**
   * Returns true if the process has been started already. All processes are started automatically when the step() method of the
   * simulation is invoked. You should never use this method.
   *
   * @return True if the process has been started already, false otherwise.
   */
//  protected synchronized final boolean hasBeenStarted()
//  {
//    return this.haveIBeenStarted;
//  } // hasBeenStarted


// ------------------------------------------------------------------------------------------------------------------------------------

  /**
   * Returns the process's number. The number is unique within the whole simulation.
   *
   * @return The process's number.
   */
  public final long getProcessNumber()
  {
    return this.myNumber;
  } // getProcessNum

  /**
   * Returns the process's name.
   *
   * @return The process's name.
   */
  public String getProcessName()
  {
    return this.name;
  } // getProcessName

  /**
   * Returns the process's state. Possible values are new, passive, scheduled, active, and terminated.
   *
   * @return The process's state.
   */
  @Override
  public synchronized final JSimProcessState getProcessState()
  {
    return this.myState;
  } // getProcessState

  /**
   * Returns the time for which is the process scheduled. If the process is not scheduled, JSimCalendar.NEVER is returned.
   *
   * @return Time for which is the process scheduled or JSimCalendar.NEVER.
   */
  public final JSimTime getScheduleTime()
  {
    return this.scheduleTime.iterator().next();
  } // getProcessNum

  /**
   * Switches the process to a new state. You should never use this method since this is an internal one. You should have a very serious
   * reason to override it.
   *
   * @param newState
   *            A new state that the process has to be switched to.
   *
   * @exception JSimInvalidProcessStateException
   *                This exception is thrown out if the process is not allowed to change its state from the current one to the new one.
   */
  @Override
  public synchronized void setProcessState(final JSimProcessState newState) throws JSimInvalidProcessStateException
  {
    final String errorLocation = "JSimProcess.setProcessState(): newState";
    boolean canSwitch = false;

    switch (this.myState)
    {
      case NEW:
        // NEW -> PASSIVE:    getReady() without activation before being started
        // NEW -> SCHEDULED:  activate() before being started (between its creation and next step())
        // NEW -> TERMINATED: simulation shutdown without the step() method being called after this process creation
        if ((newState == JSimProcessState.PASSIVE) || (newState == JSimProcessState.SCHEDULED) || (newState == JSimProcessState.TERMINATED)) {
          canSwitch = true;
        }
        break; // NEW

      case PASSIVE:
        // PASSIVE -> SCHEDULED:  activate() called
        // PASSIVE -> TERMINATED: simulation shutdown when sleeping in passivate() or getReady() -- terminated via JSimProcessDeath
        if ((newState == JSimProcessState.SCHEDULED) || (newState == JSimProcessState.TERMINATED)) {
          canSwitch = true;
        }
        break; // PASSIVE

      case SCHEDULED:
        // SCHEDULED -> PASSIVE:    cancel() invoked
        // SCHEDULED -> ACTIVE:     picked up from the calendar and restarted from getReady(), hold(), passivate(), or reactivate()
        // SCHEDULED -> TERMINATED: simulation shutdown when sleeping in hold() -- terminated via JSimProcessDeath
        if ((newState == JSimProcessState.PASSIVE) || (newState == JSimProcessState.ACTIVE) || (newState == JSimProcessState.TERMINATED)) {
          canSwitch = true;
        }
        break; // SCHEDULED

      case ACTIVE:
        // ACTIVE -> PASSIVE:                      passivate() invoked
        // ACTIVE -> SCHEDULED:                    hold() invoked
        // ACTIVE -> BLOCKED_ON_SEMAPHORE:         blockOnSemaphore() invoked via a semaphore's P()
        // ACTIVE -> BLOCKED_ON_MESSAGE_SEND:      sendMessageWithBlocking()
        // ACTIVE -> BLOCKED_ON_MESSAGE_RECEIVE:   receiveMessageWithBlocking()
        // ACTIVE -> TERMINATED:                   natural death -- switched in finish()
        if
        (
            (newState == JSimProcessState.PASSIVE) ||
            (newState == JSimProcessState.SCHEDULED) ||
            (newState == JSimProcessState.BLOCKED_ON_SEMAPHORE) ||
            (newState == JSimProcessState.TERMINATED) ||
            (newState == JSimProcessState.BLOCKED_ON_MESSAGE_SEND) ||
            (newState == JSimProcessState.BLOCKED_ON_MESSAGE_RECEIVE)
            ) {
          canSwitch = true;
        }
        break; // ACTIVE

      case BLOCKED_ON_SEMAPHORE:
        // BLOCKED_ON_SEMAPHORE -> SCHEDULED:  unblockFromSemaphore() invoked via a semaphore's V()
        // BLOCKED_ON_SEMAPHORE -> TERMINATED: simulation shutdown when sleeping in blockOnSemaphore() -- terminated via JSimProcessDeath
        if ((newState == JSimProcessState.SCHEDULED) || (newState == JSimProcessState.TERMINATED)) {
          canSwitch = true;
        }
        break; // BLOCKED_ON_SEMAPHORE

      case BLOCKED_ON_MESSAGE_SEND:
        // BLOCKED_ON_MESSAGE_SEND -> SCHEDULED:  another process invokes receiveMessageXXX() and the sender/receiver matches
        // BLOCKED_ON_MESSAGE_SEND -> TERMINATED: simulation shutdown when sleeping in sendMessageWithBlocking() -- terminated via JSimProcessDeath
        if ((newState == JSimProcessState.SCHEDULED) || (newState == JSimProcessState.TERMINATED)) {
          canSwitch = true;
        }
        break; // BLOCKED_ON_MESSAGE_SEND

      case BLOCKED_ON_MESSAGE_RECEIVE:
        // BLOCKED_ON_MESSAGE_RECEIVE -> SCHEDULED:  another process invokes sendMessageXXX() and the sender/receiver matches
        // BLOCKED_ON_MESSAGE_RECEIVE -> TERMINATED: simulation shutdown when sleeping in receiveMessageWithBlocking() -- terminated via JSimProcessDeath
        if ((newState == JSimProcessState.SCHEDULED) || (newState == JSimProcessState.TERMINATED)) {
          canSwitch = true;
        }
        break; // BLOCKED_ON_MESSAGE_RECEIVE

        // Do not do anything in TERMINATED
      case TERMINATED:
        break;

        // Do not do anything in an unknown state
      default:
        break;
    } // switch

    if (canSwitch == true) {
      this.myState = newState;
    } else {
      throw new JSimInvalidProcessStateException(errorLocation, this.myState, newState);
    }
  } // setProcessState

  /**
   * Returns true if the process is idle, false otherwise. A process is idle if it is not currently running and can be freely activated.
   * Actually, the process is idle if it is in the passive or new state.
   *
   * @return True if the process is idle, false otherwise.
   */
  public boolean isIdle()
  {
    final JSimProcessState state = this.getProcessState();
    return state == JSimProcessState.NEW || state == JSimProcessState.PASSIVE;
  } // isIdle

  /**
   * Returns the process's state as string.
   *
   * @return The process's state as string.
   */
  public String getProcessStateAsString()
  {
    return this.getProcessState().toString();
  } // getProcessStateAsString

// ------------------------------------------------------------------------------------------------------------------------------------

  /**
   * The main switching routine, responsible for switching between the main simulation thread and the process and back. When called, it
   * wakes the main thread up and passivates itself. Then it waits passivated until the main thread performing the simulation's step()
   * method sends a signal to the process via notify(). <em>You should never call this method directly.</em> It is used internally by
   * J-Sim in methods like passivate(), hold(), or any other method blocking the currently active simulation process.
   *
   * @param requestedBlockingState
   *            The process will be set to this state during its passivation. Pay attention! The setState() method must allow to set the
   *            new state!
   * @param callingMethodName
   *            The name of the method calling the switching routine.
   *
   * @exception JSimSecurityException
   *                This exception is thrown out when the process to be switched over is not currently active or if another process calls
   *                this process's method. Note: The two conditions should be equivalent since there can be at most one active J-Sim
   *                process at one time.
   * @exception JSimProcessDeath
   *                This internal J-Sim exception is thrown out when the simulation is shutting down. <em>You should NEVER catch it!</em>
   */
  protected final void mainSwitchingRoutine(final JSimProcessState requestedBlockingState, final String callingMethodName) throws JSimSecurityException
  {
    // It is not allowed to switch to the main thread if this J-Sim process is not active.
    if (this.getProcessState() != JSimProcessState.ACTIVE) {
      throw new JSimSecurityException("JSimProcess.mainSwitchingRoutine(): This process is not active.");
    }

//    synchronized(this.myOwnLock)
//    {
    try // outer try
    {
//        if ((this.shouldTerminate) && (this.requestsToTerminate >= INT_REQUESTS_TO_IGNORE))
      {
        LOG.warn("J-Sim WARNING: You probably suppress J-Sim's mechanisms of process handling.");
        LOG.warn("J-Sim WARNING: This is very dangerous and may lead to deadlock.");
        LOG.warn("J-Sim WARNING: Please check up your source code unless you really know what you are doing.");
      } // if

//        this.myOwnLock.notify();

      // Going to sleep now.
      // The new state is a state in which the process is passive or waiting for an event:
      //  * passive
      //  * blocked on semaphore
      //  * blocked on message send
      //  * blocked on message receive
      this.setProcessState(requestedBlockingState);

      // If we have to die, this exception is propagated till run().
//        if (this.shouldTerminate)
      {
        LOG.info(this.getName() + " " + callingMethodName + ": Killed.");
        throw new JSimProcessDeath();
      } // if

//      this.setProcessState(JSimProcessState.ACTIVE);
    } // outer try
    catch (final JSimInvalidProcessStateException e2)
    {
      LOG.error("Wrong process states in the main switching routine.", e2);
      throw new JSimKernelPanicException(e2);
    } // catch
    // We are now normally returning to the calling method and then to life().
//    } // synchronized
  } // mainSwitchingRoutine

// ------------------------------------------------------------------------------------------------------------------------------------

  /**
   * Blocks the process until it is unblocked by a semaphore's V() function. Returns the control back to the parent simulation and does
   * not add any new event to the calendar. A process cannot block another process, only itself via a semaphore's P() function. Blocking
   * on a semaphore terminates the current simulation step.
   *
   * @param semaphore
   *            The semaphore on which P() function this process is getting blocked.
   *
   * @exception JSimSecurityException
   *                This exception is thrown out when the process to be blocked is not currently active or if another process calls this
   *                process's blockOnSemaphore() method. (Note: The two conditions should be equivalent.)
   * @exception JSimInvalidParametersException
   *                This exception is thrown out if the semaphore in not specified (is null) or if its parent is not this process's
   *                parent.
   * @exception JSimProcessDeath
   *                This internal J-Sim exception is thrown out when the simulation is shutting down. You should NEVER catch it!
   */
  public final void blockOnSemaphore(final JSimSemaphore semaphore) throws JSimSecurityException, JSimInvalidParametersException
  {
    // It is not allowed to block somebody else.
    if (this.getProcessState() != JSimProcessState.ACTIVE) {
      throw new JSimSecurityException("JSimProcess.blockOnSemaphore(): Only an active process can be blocked.");
    }
    if (semaphore == null) {
      throw new JSimInvalidParametersException("JSimProcess.blockOnSemaphore(): semaphore");
    }

    this.semaphoreIAmCurrentlyBlockedOn = semaphore;

    this.mainSwitchingRoutine(JSimProcessState.BLOCKED_ON_SEMAPHORE, "BlockOnSemaphore");
  } // blockOnSemaphore

  /**
   * Unblocks the process which has previously got blocked on a semaphore. Adds a new entry to the calendar with the current time. The
   * process does not start running immediately but switches to the scheduled state and waits until it is re-started by the simulation
   * from its step() method.
   *
   * @param semaphore
   *            The semaphore that this process is currently blocked on.
   *
   * @exception JSimSecurityException
   *                This exception is thrown out when the process to be unblocked is not currently blocked.
   * @exception JSimInvalidParametersException
   *                This exception is thrown out if the semaphore is null or its parent is not this process's parent or it is not the
   *                semaphore that this process is blocked on.
   */
  public final void unblockFromSemaphore(final JSimSemaphore semaphore) throws JSimSecurityException, JSimInvalidParametersException
  {
    if (this.getProcessState() != JSimProcessState.BLOCKED_ON_SEMAPHORE) {
      throw new JSimSecurityException("JSimProcess.unblockFromSemaphore(): Only a blocked process can be unblocked.");
    }
    if (semaphore == null) {
      throw new JSimInvalidParametersException("JSimProcess.unblockFromSemaphore(): semaphore");
    }
    if (semaphore != this.semaphoreIAmCurrentlyBlockedOn) {
      throw new JSimInvalidParametersException("JSimProcess.blockOnSemaphore(): This process is not blocked on the semaphore specified.");
    }

//    synchronized(this.myOwnLock)
//    {
    try
    {
      //FIXME final double currentTime = this.myParent.getCurrentTime();
      //FIXME this.myParent.addEntryToCalendar(currentTime, this);
//        this.scheduleTime = currentTime;
//        this.setProcessState(JSimProcessState.SCHEDULED);
//        this.semaphoreIAmCurrentlyBlockedOn = null;
    } // try
    catch (final JSimInvalidParametersException e1) // from addCalendarEntry
    {
      LOG.error("Cannot schedule an unblocked process.", e1);
      throw new JSimKernelPanicException(e1);
    } // catch
//      catch (final JSimInvalidProcessStateException e2) // from setProcessState
//      {
//        LOG.error("Cannot set an unblocked process's state.", e2);
//        throw new JSimKernelPanicException(e2);
//      } // catch
//    } // synchronized
  } // unblockFromSemaphore

// ------------------------------------------------------------------------------------------------------------------------------------

  /**
   * Returns the process's message box. The box is used for storing messages coming directly to the process.
   *
   * @return The process's message box.
   */
  protected JSimMessageBox getMessageBox()
  {
    return this.myMessageBox;
  } // getMessageBox

  /**
   * Indicates whether this process's clipboard is empty or not. The clipboard is used to store a single message taken out from a message
   * box when the process is blocked during a blocking send operation.
   *
   * @return True is this process's clipboard is empty, false otherwise.
   */
  @Override
  public boolean hasEmptyMessageClipboard()
  {
    return (this.messageClipboard == null);
  } // hasEmptyMessageClipboard

  /**
   * Copies a single message to the process's clipboard. The clipboard must be empty. No physical copying is done, just reference
   * assignment.
   *
   * @param message
   *            The message to be put into the clipboard.
   *
   * @exception JSimSecurityException
   *                This exception is thrown out if the clipboard is not empty.
   */
  protected void copyToMessageClipboard(final JSimMessage message) throws JSimSecurityException
  {
    if (!this.hasEmptyMessageClipboard()) {
      throw new JSimSecurityException("The clipboard is full!");
    }

    this.messageClipboard = message;
  } // copyToMessageClipboard

  /**
   * Reads a message from the clipboard and returns it. If there is no message in the clipboard, null is returned. The message is taken
   * out from the clipboard.
   *
   * @return A message stored in the clipboard or null.
   */
  protected JSimMessage readFromClipboard()
  {
    JSimMessage message;

    message = this.messageClipboard;
    this.messageClipboard = null;

    return message;
  } // readFromClipboard

  /**
   * Returns the process that this process specified as sender for its blocking receive operation. If no sender was specified or this
   * process is not performing a blocking receive operation, null is returned.
   *
   * @return The process that this process specified as sender for its blocking receive operation.
   */
  @Override
  public JSimProcessImpl getSenderIAmWaitingFor()
  {
    return this.messageSenderIAmWaitingFor;
  } // getSenderIAmWaitingFor

  /**
   * A routine used for process reactivation after a successfully completed send or receive blocking operation.
   *
   * @exception JSimSecurityException
   *                This exception is thrown out if the process is not currently blocked.
   */
  private final void unblockFromMessageSendOrReceive() throws JSimSecurityException
  {
    if ((this.getProcessState() != JSimProcessState.BLOCKED_ON_MESSAGE_SEND) && (this.getProcessState() != JSimProcessState.BLOCKED_ON_MESSAGE_RECEIVE)) {
      throw new JSimSecurityException("JSimProcess.unblockFromMessageSendOrReceive(): Only a blocked process can be unblocked.");
    }

//    synchronized(this.myOwnLock)
//    {
//      try
//      {
//        final double currentTime = this.myParent.getCurrentTime();
//        this.myParent.addEntryToCalendar(currentTime, this);
//        this.scheduleTime = currentTime;
//        this.setProcessState(JSimProcessState.SCHEDULED);
//      } // try
//      catch (final JSimInvalidParametersException e1) // from addCalendarEntry
//      {
//        LOG.error("Cannot schedule an unblocked process.", e1);
//        throw new JSimKernelPanicException(e1);
//      } // catch
//      catch (final JSimInvalidProcessStateException e2) // from setProcessState
//      {
//        LOG.error("Cannot set an unblocked process's state.", e2);
//        throw new JSimKernelPanicException(e2);
//      } // catch
//    } // synchronized
  } // unblockFromMessageSendOrReceive

// ------------------------------------------------------------------------------------------------------------------------------------

  /**
   * Sends the specified message to the process denoted as receiver of the message. If the receiver is not currently waiting for the
   * message, the sending process will block and the current simulation step will be terminated.
   *
   * @param message
   *            The message to be sent.
   *
   * @exception JSimInvalidParametersException
   *                This exception is thrown out if the message is not specified (null).
   */
  public void sendMessageWithBlocking(final JSimSymmetricMessage message) throws JSimInvalidParametersException
  {
    if (message == null) {
      throw new JSimInvalidParametersException("JSimProcess.sendMessageWithBlocking(): message");
    }

    this.sendMessageWithBlocking(message, JSimMessageService.getMessageBox(message.getReceiver()));
  } // sendMessageWithBlocking

  /**
   * Sends the specified message to the process denoted as receiver of the message. If the receiver is not currently waiting for the
   * message, the sending process will block and the current simulation step will be terminated.
   *
   * @param message
   *            The message to be sent.
   *
   * @exception JSimInvalidParametersException
   *                This exception is thrown out if the message is not specified (null).
   */
  public void sendMessageWithBlocking(final JSimMessageForReceiver message) throws JSimInvalidParametersException
  {
    if (message == null) {
      throw new JSimInvalidParametersException("JSimProcess.sendMessageWithBlocking(): message");
    }

    this.sendMessageWithBlocking(message, JSimMessageService.getMessageBox(message.getReceiver()));
  } // sendMessageWithBlocking

  /**
   * Sends the specified message to the specified message box. If there is no receiver currently waiting for the message, the sending
   * process will block and the current simulation step will be terminated.
   *
   * @param message
   *            The message to be sent.
   * @param box
   *            The message box that the message has to be put to.
   *
   * @exception JSimInvalidParametersException
   *                This exception is thrown out if the message or the the message box is not specified (null).
   */
  public void sendMessageWithBlocking(final JSimMessage message, final JSimMessageBox box)
  {
    Objects.requireNonNull(message, "JSimProcess.sendMessageWithBlocking(): message");
    Objects.requireNonNull(box, "JSimProcess.sendMessageWithBlocking(): box");

    try
    {
      final JSimProcessImpl sendingProcess = this;
      message.setRealSender(sendingProcess);
      // Is there a suspended process that is waiting for this message?
      // If there is one, give it the message directly, otherwise put the message to the box and suspend itself.
      // A process with full clipboard cannot be returned.
      // The sendingProcess argument has usually no value when receivers wait for messages from anybody.
      final JSimProcessImpl suspendedReceiver = (JSimProcessImpl) box.getFirstSuspendedReceiver(message.getReceiver(), sendingProcess);
      if (suspendedReceiver == null)
      {
        box.addMessage(message);
        box.addSuspendedSender(sendingProcess);
        LOG.debug("{} : Blocking send -- No suspended receiver found for receiver=`{}' and sender=`{}'.",
            this.getName(), message.getReceiver(), sendingProcess);
        this.mainSwitchingRoutine(JSimProcessState.BLOCKED_ON_MESSAGE_SEND, "SendMessageWithBlocking");
      } // if no suspended receiver
      else
      {
        suspendedReceiver.copyToMessageClipboard(message);
        suspendedReceiver.unblockFromMessageSendOrReceive();
      } // else no suspended receiver
    } // try
    catch (final JSimSecurityException e)
    {
      LOG.error("A serious error occured during message send.", e);
      throw new JSimKernelPanicException(e);
    } // catch
  } // sendMessageWithBlocking

// ------------------------------------------------------------------------------------------------------------------------------------

  /**
   * Sends the specified message to the process denoted as receiver of the message. If the receiver is not currently waiting for the
   * message, the sending process will just store the message to its message box. In any case, the sender returns immediately.
   *
   * @param message
   *            The message to be sent.
   *
   * @exception JSimInvalidParametersException
   *                This exception is thrown out if the message is not specified (null).
   */
  public void sendMessageWithoutBlocking(final JSimSymmetricMessage message) throws JSimInvalidParametersException
  {
    if (message == null) {
      throw new JSimInvalidParametersException("JSimProcess.sendMessageWithoutBlocking(): message");
    }

    this.sendMessageWithoutBlocking(message, JSimMessageService.getMessageBox(message.getReceiver()));
  } // sendMessageWithoutBlocking

  /**
   * Sends the specified message to the process denoted as receiver of the message. If the receiver is not currently waiting for the
   * message, the sending process will just store the message to its message box. In any case, the sender returns immediately.
   *
   * @param message
   *            The message to be sent.
   *
   * @exception JSimInvalidParametersException
   *                This exception is thrown out if the message is not specified (null).
   */
  public void sendMessageWithoutBlocking(final JSimMessageForReceiver message) throws JSimInvalidParametersException
  {
    if (message == null) {
      throw new JSimInvalidParametersException("JSimProcess.sendMessageWithoutBlocking(): message");
    }

    this.sendMessageWithoutBlocking(message, JSimMessageService.getMessageBox(message.getReceiver()));
  } // sendMessageWithoutBlocking

  /**
   * Sends the specified message to the specified message box. If there is no receiver currently waiting for the message, the sending
   * process will just store the message to the message box. In any case, the sender returns immediately.
   *
   * @param message
   *            The message to be sent.
   * @param box
   *            The message box that the message has to be put to.
   *
   * @exception JSimInvalidParametersException
   *                This exception is thrown out if the message or the the message box is not specified (null).
   */
  public void sendMessageWithoutBlocking(final JSimMessage message, final JSimMessageBox box) throws JSimInvalidParametersException
  {
    JSimProcessImpl sendingProcess;
    JSimProcessImpl suspendedReceiver;

    if (message == null) {
      throw new JSimInvalidParametersException("JSimProcess.sendMessageWithoutBlocking(): message");
    }
    if (box == null) {
      throw new JSimInvalidParametersException("JSimProcess.sendMessageWithoutBlocking(): box");
    }

    try
    {
      sendingProcess = this;
      message.setRealSender(sendingProcess);
      // Is there a suspended process that is waiting for this message?
      // If there is one, give it the message directly, otherwise put the message to the box.
      // A process with full clipboard cannot be returned.
      if ((suspendedReceiver = (JSimProcessImpl) box.getFirstSuspendedReceiver(message.getReceiver(), sendingProcess)) == null) {
        box.addMessage(message);
      } else
      {
        suspendedReceiver.copyToMessageClipboard(message);
        suspendedReceiver.unblockFromMessageSendOrReceive();
      } // else no suspended receiver
    } // try
    catch (final JSimSecurityException e)
    {
      LOG.error("A serious error occured during message send.", e);
      throw new JSimKernelPanicException(e);
    } // catch
  } // sendMessageWithoutBlocking

// ------------------------------------------------------------------------------------------------------------------------------------

  /**
   * Receives and returns a message previously sent to this process by any other process. If there is no message in the receiver's message
   * box currently waiting for the receiver, the receiver blocks and the current simulation step is terminated.
   *
   * @return A message read from the receiver's message box.
   */
  public JSimMessage receiveMessageWithBlocking()
  {
    try
    {
      return this.receiveMessageWithBlocking(this.getMessageBox(), JSimMessage.UNKNOWN_SENDER);
    } // try
    catch (final JSimInvalidParametersException e)
    {
      LOG.error("An invalid message box provided by a process.", e);
      // The exception is thrown out because of an invalid message box --> J-Sim problem.
      throw new JSimKernelPanicException(e);
    } // catch
  } // receiveMessageWithBlocking

  /**
   * Receives and returns a message from the specified message box. If there is no message in the message box currently waiting for the
   * receiver, the receiver blocks and the current simulation step is terminated.
   *
   * @param box
   *            The message box that the message has to be read from.
   *
   * @return A message read from the specified message box previously sent by the specified sender.
   *
   * @exception JSimInvalidParametersException
   *                This exception is thrown out if the message box is not specified.
   */
  public JSimMessage receiveMessageWithBlocking(final JSimMessageBox box) throws JSimInvalidParametersException
  {
    if (box == null) {
      throw new JSimInvalidParametersException("JSimProcess.receiveMessageWithBlocking(): box");
    }

    // Just ignoring JSimInvalidParametersException and passing it to the user who supplied the box.
    // The exception can be thrown out just because of an invalid message box.
    return this.receiveMessageWithBlocking(box, JSimMessage.UNKNOWN_SENDER);
  } // receiveMessageWithBlocking

  /**
   * Receives and returns a message previously sent by the specified sender to this process. The sender need not be specified. If it is
   * not specified (JSimMessage.UNKNOWN_SENDER), message from any sender can be returned. If there is no message in the message box of the
   * receiver, currently waiting for the receiver and sent by the sender, the receiver blocks and the current simulation step is
   * terminated.
   *
   * @param sender
   *            The sender of the message. Need not be specified.
   *
   * @return A message read from the receiver's message box previously sent by the specified sender.
   */
  public JSimMessage receiveMessageWithBlocking(final JSimProcessImpl sender)
  {
    try
    {
      return this.receiveMessageWithBlocking(this.getMessageBox(), sender);
    } // try
    catch (final JSimInvalidParametersException e)
    {
      LOG.error("An invalid message box provided by a process.", e);
      // The exception is thrown out because of an invalid message box --> J-Sim problem.
      throw new JSimKernelPanicException(e);
    } // catch
  } // receiveMessageWithBlocking

  /**
   * Receives and returns a message from the specified message box previously sent by the specified sender. The sender need not be
   * specified. If it is not specified (JSimMessage.UNKNOWN_SENDER) message from any sender can be returned. If there is no message in the
   * message box currently waiting for the receiver and sent by the sender, the receiver blocks and the current simulation step is
   * terminated.
   *
   * @param box
   *            The message box that the message has to be read from.
   * @param sender
   *            The sender of the message. Need not be specified.
   *
   * @return A message read from the specified message box previously sent by the specified sender.
   *
   * @exception JSimInvalidParametersException
   *                This exception is thrown out if the message box is not specified.
   */
  public JSimMessage receiveMessageWithBlocking(final JSimMessageBox box, final JSimConversationable sender) throws JSimInvalidParametersException
  {
    JSimMessage message = null;
    JSimProcess realSender;

    if (box == null) {
      throw new JSimInvalidParametersException("JSimProcess.receiveMessageWithBlocking(): box");
    }

    try
    {
      // If there is no message in the box for this process from the specified sender (or anybody),
      // suspend itself, otherwise take from the box and activate the sender, if it is suspended.
      if ((message = box.getFirstMessageFromAndFor(sender, this)) != null)
      {
        realSender = message.getRealSender();
        if (box.containsSuspendedSender(realSender)) {
//          realSender.unblockFromMessageSendOrReceive();
        }
      } // if message for me
      else
      {
        LOG.debug("{}: Blocking receive -- No appropriate message found for receiver=`{}' in message box=`{}'.",
            this.getName(), this, box);
        box.addSuspendedReceiver(this);
        this.mainSwitchingRoutine(JSimProcessState.BLOCKED_ON_MESSAGE_RECEIVE, "ReceiveMessageWithBlocking");
        // Now I am woken up and there is a message from a sender in my clipboard.
        message = this.readFromClipboard();
      } // else message for me
    } // try
    catch (final JSimSecurityException e)
    {
      LOG.error("An error occured during message receive.", e);
      throw new JSimKernelPanicException(e);
    } // catch

    return message;
  } // receiveMessageWithBlocking

// ------------------------------------------------------------------------------------------------------------------------------------

  /**
   * Receives and returns a message from the receiver's message box previously sent by any process. If there is no message currently
   * waiting for the receiver, the method returns null. In any case, the receiver returns immediately from this method.
   *
   * @return A message read from the receiver's message box.
   */
  public JSimMessage receiveMessageWithoutBlocking()
  {
    try
    {
      return this.receiveMessageWithoutBlocking(this.getMessageBox(), JSimMessage.UNKNOWN_SENDER);
    } // try
    catch (final JSimInvalidParametersException e)
    {
      LOG.error("An invalid message box provided by a process.", e);
      // The exception is thrown out because of an invalid message box --> J-Sim problem.
      throw new JSimKernelPanicException(e);
    } // catch
  } // receiveMessageWithoutBlocking

  /**
   * Receives and returns a message from the specified message box previously sent by any process. If there is no message in the message
   * box currently waiting for the receiver, the method returns null. In any case, the receiver returns immediately from this method.
   *
   * @param box
   *            The message box that the message has to be read from.
   *
   * @return A message read from the specified message box.
   *
   * @exception JSimInvalidParametersException
   *                This exception is thrown out if the message box is not specified.
   */
  public JSimMessage receiveMessageWithoutBlocking(final JSimMessageBox box) throws JSimInvalidParametersException
  {
    if (box == null) {
      throw new JSimInvalidParametersException("JSimProcess.receiveMessageWithoutBlocking(): box");
    }

    // Just ignoring JSimInvalidParametersException and passing it to the user who supplied the box.
    // The exception can be thrown out just because of an invalid message box.
    return this.receiveMessageWithoutBlocking(box, JSimMessage.UNKNOWN_SENDER);
  } // receiveMessageWithoutBlocking

  /**
   * Receives and returns a message from the receiver's message box previously sent by the specified sender. The sender need not be
   * specified. If it is not specified (JSimMessage.UNKNOWN_SENDER) message from any sender can be returned. If there is no message in the
   * message box currently waiting for the receiver and sent by the sender, the method returns null. In any case, the receiver returns
   * immediately from this method.
   *
   * @param sender
   *            The sender of the message. Need not be specified.
   *
   * @return A message read from the receiver's message box previously sent by the specified sender.
   */
  public JSimMessage receiveMessageWithoutBlocking(final JSimProcessImpl sender)
  {
    try
    {
      return this.receiveMessageWithoutBlocking(this.getMessageBox(), sender);
    } // try
    catch (final JSimInvalidParametersException e)
    {
      LOG.error("An invalid message box provided by a process.", e);
      // The exception is thrown out because of an invalid message box --> J-Sim problem.
      throw new JSimKernelPanicException(e);
    } // catch
  } // receiveMessageWithoutBlocking

  /**
   * Receives and returns a message from the specified message box previously sent by the specified sender. The sender need not be
   * specified. If it is not specified (JSimMessage.UNKNOWN_SENDER) message from any sender can be returned. If there is no message in the
   * message box currently waiting for the receiver and sent by the sender, the method returns null. In any case, the receiver returns
   * immediately from this method.
   *
   * @param box
   *            The message box that the message has to be read from.
   * @param sender
   *            The sender of the message. Need not be specified.
   *
   * @return A message read from the specified message box previously sent by the specified sender.
   *
   * @exception JSimInvalidParametersException
   *                This exception is thrown out if the message box is not specified.
   */
  public JSimMessage receiveMessageWithoutBlocking(final JSimMessageBox box, final JSimProcess sender) throws JSimInvalidParametersException
  {
    Objects.requireNonNull(box, "JSimProcess.receiveMessageWithoutBlocking(): box");

//    try
//    {
    JSimMessage message = null;
    // If there is no message in the box for this process from the specified sender (or anybody),
    // return null, otherwise take from the box and activate the sender, if it is suspended.
    if ((message = box.getFirstMessageFromAndFor(sender, this)) != null)
    {
      final JSimProcess realSender = message.getRealSender();
      if (box.containsSuspendedSender(realSender)) {
//          realSender.unblockFromMessageSendOrReceive();
      }
    } // if message for me
    return message;
//    } // try
//    catch (final JSimSecurityException e)
//    {
//      LOG.error("An error occured during message receive.", e);
//      throw new JSimKernelPanicException(e);
//    } // catch

  } // receiveMessageWithoutBlocking

// ------------------------------------------------------------------------------------------------------------------------------------

  /**
   * Prints out a text info message, either to the standard output or to the simulation window.
   *
   * @param s
   *            The message to be printed out.
   */
  public void message(final String s)
  {
    System.err.println(s);
//    this.myParent.printString(s, JSimSimulation.PRN_MESSAGE, true);
  } // message

  /**
   * Prints a text info message, either to the standard output or to the simulation window, but does not teminate the line.
   *
   * @param s
   *            The message to be printed out.
   */
  public void messageNoNL(final String s)
  {
//    this.myParent.printString(s, JSimSimulation.PRN_MESSAGE, false);
  } // message

  /**
   * Prints out a text error message, either to the error output or to the simulation window.
   *
   * @param s
   *            The error message to be printed out.
   */
  public void error(final String s)
  {
//    this.myParent.printString(s, JSimSimulation.PRN_ERROR, true);
  } // error

// ------------------------------------------------------------------------------------------------------------------------------------

  /**
   * Returns a string containing basic information about the process. The string can be displayed in a JSimMainWindowList component.
   *
   * @return A string containing basic information about the process.
   */
  @Override
  public String getObjectListItemDescription()
  {
    return this.toString();
  } // getObjectListItemDescription

  /**
   * Returns a collection of process's characteristics. Every characteristics contains a name and a value. The collection can be displayed
   * in a JSimDetailedInfoWindow table.
   *
   * @return A collection of JSimPairs.
   */
  @Override
  public Collection<JSimPair> getDetailedInformationArray()
  {
    final Collection<JSimPair> c = new ArrayList<>(4);
    c.add(new JSimPair("Number:", Long.toString(this.getProcessNumber())));
    c.add(new JSimPair("Name:", this.getName()));
    c.add(new JSimPair("State:", this.getProcessStateAsString()));
    if (this.myState == JSimProcessState.SCHEDULED) {
      c.add(new JSimPair("Scheduled for:", this.scheduleTime));
    }
    if (this.myState == JSimProcessState.BLOCKED_ON_SEMAPHORE) {
      c.add(new JSimPair("Semaphore:", this.semaphoreIAmCurrentlyBlockedOn.getSemaphoreName()));
    }
    return c;
  } // getDetailedInformationArray

  /**
   * Creates a detailed info window that shows information about this process. Returns a reference to the created window.
   *
   * @return Reference to the created info window.
   */
  @Override
  public JDialog createDetailedInfoWindow(final JSimMainWindow parentWindow)
  {
    final JSimDetailedInfoWindow dWindow = new JSimDetailedInfoWindow(parentWindow, this);
    return dWindow;
  } // createDetailedInfoWindow

// ------------------------------------------------------------------------------------------------------------------------------------

  /**
   * Returns a string representation of the process. Provided information: number, name, state, and schedule time.
   *
   * @return A string representation of the process.
   */
  @Override
  public String toString()
  {
    final String s = this.getName()
        + "\n\tTq: " + this.responseTimeStatistics.getMean()
        + "\n\tLoad: " + this.totalProcessingTime / this.simulationTime.getTime()
        ;

//    if (this.getProcessState() == JSimProcessState.SCHEDULED) {
//      s += " for " + this.getScheduleTime() + "[" + this.getSimulationTime() + "]";
//    }
//
//    if (this.getProcessState() == JSimProcessState.BLOCKED_ON_SEMAPHORE) {
//      s += " " + this.semaphoreIAmCurrentlyBlockedOn.getSemaphoreName();
//    }

    return s;
  } // toString

  /**
   * Compares this process with another one. Returns a negative integer, zero, or a positive integer as this process is less than, equal
   * to, or greater than the specified object. It is assumed that the argument is also a JSimProcess. This class has a natural ordering
   * that is fully consistent with equals(). If equals() returns true for p1 and p2, then compareTo() will return 0 for the same p1 and
   * p2, and vice versa.
   *
   * @return Zero if the numbers of both processes are equal, a negative number if the number of this process is less than the other
   *         process's number, and a positive number if the number of this process is greater than the other process's number.
   *
   * @exception ClassCastException
   *                This exception is thrown out when the specified object cannot be typecasted to JSimProcess.
   */
  @Override
  public int compareTo(final JSimProcessImpl p)
  {
//    if (this.myParent.getSimulationNumber() == p.myParent.getSimulationNumber()) {
//      if (this.myNumber == p.myNumber) {
//        return 0;
//      } else
    if (this.myNumber < p.myNumber) {
      return -1;
    } else {
      return +1;
    }
//    } else
//      if (this.myParent.getSimulationNumber() < p.myParent.getSimulationNumber()) {
//        return -1;
//      } else {
//        return +1;
//      }
  } // compareTo

  /**
   * Indicates whether some other object is equal to this one. This implementation compares process numbers and their simulations' numbers
   * which is actually equal to simple reference comparison because process numbers are unique for a given simulation and simulation
   * numbers are unique for a given JVM instance. Unique process numbers are assured by the constructor and the
   * JSimSimulation.getFreeProcessNumber() method. Unique simulation numbers are assured by the JSimSimulation constructor.
   *
   * @param o
   *            The reference object with which to compare.
   *
   * @return True if this object is the same as the obj argument, false otherwise.
   */
  @Override
  public boolean equals(final Object o)
  {
    if (o == this) {
      return true;
    }

    if ((o instanceof JSimProcessImpl) == false) {
      return false;
    }

    final JSimProcessImpl p = (JSimProcessImpl) o;

    return this.myNumber == p.myNumber;
//        && this.myParent.getSimulationNumber() == p.myParent.getSimulationNumber();
  } // equals

  /**
   * Returns a hash code value for the object. The hash code is computed from the process's number and its simulation's number using the
   * algorithm described in [UJJ3/166]. This implementation of hash code computation is fully consistent with equals().
   *
   * @return A hash code for this process.
   */
  @Override
  public int hashCode()
  {
    int temp = 17; // Magic number 17
    final int myNumberAsInt = (int) (this.myNumber ^ (this.myNumber >>> 32));
//    final int simulationNumber = this.myParent.getSimulationNumber();

    temp = 37 * temp + myNumberAsInt; // Another magic number 37
//    temp = 37 * temp + simulationNumber;

    return temp;
  } // hashCode

  /**
   * @return
   */
  public boolean isAlive() {
    // TODO Auto-generated method stub
    return false;
  }

  /* (non-Javadoc)
   * @see cz.zcu.fav.kiv.jsim.core.JSimTask#getScheduler()
   */
  @Override
  public Optional<JSimScheduler> getScheduler() {
    return Optional.of(new Scheduler());
  }

  /* (non-Javadoc)
   * @see cz.zcu.fav.kiv.jsim.core.JSimPlannable#setTime(cz.zcu.fav.kiv.jsim.core.JSimTimeFactory.JSimTime)
   */
  @Override
  public void setTime(final JSimTime time) {
    this.setSimulationTime(time);
  }

  /**
   * @return the simulationTime
   */
  public JSimTime getSimulationTime() {
    return this.simulationTime;
  }

  /**
   * @param simulationTime the simulationTime to set
   */
  public void setSimulationTime(final JSimTime simulationTime) {
    this.simulationTime = simulationTime;
  }

  @Override
  public void setSheduledFor(final JSimTime time) {
    this.scheduleTime.add(time);
  }

  /**
   * @return the simulationId
   */
  public UUID getSimulationId() {
    LOG.trace("getSimulationId()");
    return this.simulationId;
  }

  @Override
  public void setSimulationId(final UUID simulationId) {
    LOG.trace("setSimulationId(simulationId={})", simulationId);
    if (this.simulationId != null) {
      throw new JSimInvalidParametersException(simulationId.toString());
    }
    this.simulationId  = simulationId;
  }



} // class JSimProcess
