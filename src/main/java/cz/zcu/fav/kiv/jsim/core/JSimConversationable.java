package cz.zcu.fav.kiv.jsim.core;

/**
 * JSimConversationable.java
 *    31. 10. 2015
 * @author Lukáš Kvídera
 * @version 0.0
 */
public interface JSimConversationable {
  JSimProcess getSenderIAmWaitingFor();
  boolean hasEmptyMessageClipboard();
}
