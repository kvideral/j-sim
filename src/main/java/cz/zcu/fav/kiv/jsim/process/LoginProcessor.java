package cz.zcu.fav.kiv.jsim.process;

import java.util.function.Consumer;
import java.util.function.Supplier;

import javax.annotation.PostConstruct;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import cz.zcu.fav.kiv.jsim.core.JSimProcessImpl;
import cz.zcu.fav.kiv.jsim.link.JSimObject;
import cz.zcu.fav.kiv.jsim.link.JSimProbabilitySplitterFactory;
import cz.zcu.fav.kiv.jsim.link.JSimQueueFactory;
import cz.zcu.fav.kiv.jsim.link.JSimQueueFactoryManager;
import cz.zcu.fav.kiv.jsim.random.JSimDistribution;


/**
 * LoadBalancer.java
 *    6. 12. 2015
 * @author Lukáš Kvídera
 * @version 0.0
 */
public class LoginProcessor extends JSimProcessImpl {

  private static final Logger LOG = LoggerFactory.getLogger(LoginProcessor.class);

  private Supplier<JSimObject> loginQueue;
  private Consumer<JSimObject> databasePageSplitter;

  public LoginProcessor(final String name, final JSimDistribution distribution) {
    super(name, distribution);
  }

  @PostConstruct
  public void init() {
    LOG.trace("init()");
    this.databasePageSplitter = JSimProbabilitySplitterFactory.createSplitter(
        0.5,
        this.getSimulationId(),
        "databaseQueue",
        "pageQueue"
        );

    final JSimQueueFactory factory = JSimQueueFactoryManager.getInstance(this.getSimulationId());
    this.loginQueue = factory.lookup("loginQueue");
  }

  @Override
  protected JSimObject serve() {
    return this.loginQueue.get();
  }

  @Override
  public void doWork(final JSimObject jSimObject) {
    LOG.trace("doWork(jSimObject={})", jSimObject);
    this.databasePageSplitter.accept(jSimObject);
  }

}
