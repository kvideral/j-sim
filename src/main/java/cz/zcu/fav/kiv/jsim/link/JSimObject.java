package cz.zcu.fav.kiv.jsim.link;

import java.util.concurrent.atomic.AtomicLong;

import cz.zcu.fav.kiv.jsim.link.JSimTimeFactory.JSimTime;

public class JSimObject {

  private static final AtomicLong counter = new AtomicLong();
  private static final JSimObject EMPTY = new JSimObject(JSimTime.valueOf(0));

  private final long id = counter.incrementAndGet();

  private final JSimTime creationTime;

  /**
   * The simulation time that this link entered its current queue.
   */
  private JSimTime enterTime;

  public JSimObject(final JSimTime creationTime) {
    this.creationTime = JSimTime.valueOf(creationTime);
  }


  public static JSimObject empty() {
    return EMPTY;
  }

  /**
   * @return the creationTime
   */
  public JSimTime getCreationTime() {
    return this.creationTime;
  }

  /**
   * @return the enterTime
   */
  public JSimTime getEnterTime() {
    return this.enterTime;
  }

  /**
   * @param enterTime the enterTime to set
   */
  public void setEnterTime(final double enterTime) {
    this.enterTime = JSimTime.valueOf(enterTime);
  }

  @Override
  public String toString() {
    return "JSimObject [id=" + this.id
        + ", enterTime=" + this.enterTime
        + ", cretad=" + this.creationTime
        + "]";
  }

}
