package cz.zcu.fav.kiv.jsim.process;

import javax.annotation.PostConstruct;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import cz.zcu.fav.kiv.jsim.core.JSimProcessImpl;
import cz.zcu.fav.kiv.jsim.core.iface.JSimQueue;
import cz.zcu.fav.kiv.jsim.link.JSimObject;
import cz.zcu.fav.kiv.jsim.link.JSimQueueFactory;
import cz.zcu.fav.kiv.jsim.link.JSimQueueFactoryManager;
import cz.zcu.fav.kiv.jsim.random.JSimDistribution;


/**
 * ProducerProcess.java
 *    6. 11. 2015
 * @author Lukáš Kvídera
 * @version 0.0
 */
public class ProducerProcess extends JSimProcessImpl {

  private static final Logger LOG = LoggerFactory.getLogger(ProducerProcess.class);
  private JSimQueue outputQueue;

  public ProducerProcess(final String name, final JSimDistribution distribution) {
    super(name, distribution);
  }

  @PostConstruct
  public void init() {
    LOG.trace("init()");
    final JSimQueueFactory instance = JSimQueueFactoryManager.getInstance(this.getSimulationId());
    this.outputQueue = instance.lookup("Consumers input queue");
  }

  @Override
  protected JSimObject serve() {
    return new JSimObject(this.getSimulationTime());
  }

  @Override
  public void doWork(final JSimObject jSimObject) {
    LOG.trace("call() It's {} o'clock", this.getSimulationTime());
    this.outputQueue.accept(jSimObject);
  }

}
