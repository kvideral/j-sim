package cz.zcu.fav.kiv.jsim.process;

import java.util.function.Consumer;
import java.util.function.Supplier;

import javax.annotation.PostConstruct;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import cz.zcu.fav.kiv.jsim.core.JSimProcessImpl;
import cz.zcu.fav.kiv.jsim.link.JSimFlowMeasurePointFactory;
import cz.zcu.fav.kiv.jsim.link.JSimObject;
import cz.zcu.fav.kiv.jsim.link.JSimQueueFactory;
import cz.zcu.fav.kiv.jsim.link.JSimQueueFactoryManager;
import cz.zcu.fav.kiv.jsim.random.JSimDistribution;


/**
 * LoadBalancer.java
 *    6. 12. 2015
 * @author Lukáš Kvídera
 * @version 0.0
 */
public class DatabaseCenter extends JSimProcessImpl {

  private static final Logger LOG = LoggerFactory.getLogger(DatabaseCenter.class);

  private Supplier<JSimObject> databaseQueue;
  private Consumer<JSimObject> mainInputQueue;

  public DatabaseCenter(final String name, final JSimDistribution distribution) {
    super(name, distribution);
  }

  @PostConstruct
  public void init() {
    LOG.trace("init()");
    final JSimQueueFactory factory = JSimQueueFactoryManager.getInstance(this.getSimulationId());
    this.databaseQueue = factory.lookup("databaseQueue");
    this.mainInputQueue = JSimFlowMeasurePointFactory.createMeasurePoint(this.getSimulationId(),
        "Datrabase flow", "mainInputQueue");
  }

  @Override
  protected JSimObject serve() {
    return this.databaseQueue.get();
  }

  @Override
  public void doWork(final JSimObject jSimObject) {
    LOG.trace("doWork()");
    this.mainInputQueue.accept(jSimObject);
  }

}
