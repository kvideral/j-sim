package cz.zcu.fav.kiv.jsim.random;


/**
 * JSimConstantDistribution.java
 *    11. 12. 2015
 * @author Lukáš Kvídera
 * @version 0.0
 */
public class JSimConstantDistribution implements JSimDistribution {

  private final double interval;

  public JSimConstantDistribution(final double interval) {
    this.interval = interval;
  }

  @Override
  public double next() {
    return this.interval;
  }

}
