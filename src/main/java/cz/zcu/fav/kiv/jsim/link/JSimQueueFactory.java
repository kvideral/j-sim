package cz.zcu.fav.kiv.jsim.link;

import java.util.Collection;
import java.util.LinkedList;
import java.util.UUID;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentMap;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import cz.zcu.fav.kiv.jsim.core.iface.JSimQueue;
import cz.zcu.fav.kiv.jsim.link.JSimTimeFactory.JSimTime;

/**
 * JSimQueueFactory.java
 *    6. 11. 2015
 * @author Lukáš Kvídera
 * @version 1.0
 */
public class JSimQueueFactory {

  private final ConcurrentMap<String, JSimQueue> queueCache = new ConcurrentHashMap<>();
  protected final JSimTime globalTime;

  public class JSimQueueImpl implements JSimQueue {
    private final Logger LOG = LoggerFactory.getLogger(JSimQueueFactory.JSimQueueImpl.class);

    private final JSimStatistics timeWaitStatistics = new JSimStatistics();
    private final LinkedList<JSimObject> internalQueue = new LinkedList<>();
    private final String name;
    private JSimTime lastChange = JSimTime.valueOf(0);
    private double sumLwUntilLastChange;
    private double errorCompensation;

    private JSimQueueImpl(final String name) {
      this.name = name;
    }

    private void recalculateLw() {
      this.LOG.trace("recalculateLw()");
      final double now = JSimQueueFactory.this.globalTime.getTime();
      this.LOG.debug("recalculateLw(\nnow={},\nlastChange={},\numLwUntilLastChange={})\necc={}",
          now, this.lastChange, this.sumLwUntilLastChange, this.errorCompensation);

      final double weightedWaitingTime = (now - this.lastChange.getTime()) * this.internalQueue.size();
      final double diff = weightedWaitingTime - this.errorCompensation;
      final double temporarySum = this.sumLwUntilLastChange + diff;
      this.errorCompensation = (temporarySum - this.sumLwUntilLastChange) - diff;
      this.sumLwUntilLastChange = temporarySum;
      this.lastChange = JSimTime.valueOf(now);
    }

    private void recalculateTw(final JSimTime whenEntered) {
      this.timeWaitStatistics.accept(JSimQueueFactory.this.globalTime.getTime() - whenEntered.getTime());
    }

    @Override
    public void accept(final JSimObject t) {
      this.LOG.trace("accept(t={})", t);
      t.setEnterTime(JSimQueueFactory.this.globalTime.getTime());
      this.internalQueue.addLast(t);
      this.recalculateLw();
    }

    @Override
    public JSimObject get() {
      this.LOG.trace("get()");
      if (this.internalQueue.isEmpty()) {
        return null;
      }

      final JSimObject first = this.internalQueue.removeFirst();
      this.recalculateLw();
      this.recalculateTw(first.getEnterTime());
      return first;
    }

    @Override
    public double getLw() {
      // We must count the result without affecting the statistics
      final JSimTime now = JSimQueueFactory.this.globalTime;
      final double tempSumLw = this.sumLwUntilLastChange + ((now.getTime() - this.lastChange.getTime()) * this.internalQueue.size()); // The stored value + delta since last insertion/deletion

      return tempSumLw / now.getTime();
    }

    @Override
    public double getTw() {
      return this.timeWaitStatistics.getMean();
    }

    @Override
    public String toString() {
      return this.name
          + "\n\tLw=" + this.getLw()
          + "\n\tTw=" + this.getTw()
          + "\n\tsize=" + this.internalQueue.size()
          ;
    }

  }

  private class SinkHole implements JSimQueue {

    private final Logger LOG = LoggerFactory.getLogger(JSimQueueFactory.SinkHole.class);

    private final JSimStatistics totalResponseTime = new JSimStatistics();

    @Override
    public void accept(final JSimObject t) {
      this.LOG.trace("accept(t={})", t);
      this.totalResponseTime.accept(JSimQueueFactory.this.globalTime.getTime() - t.getCreationTime().getTime());
    }

    @Override
    public JSimObject get() {
      return null;
    }

    @Override
    public double getLw() {
      return 0.0;
    }

    @Override
    public double getTw() {
      return 0.0;
    }

    @Override
    public String toString() {
      return "SinkHole"
          + "\n\tTq=" + this.totalResponseTime.getMean()
          + " (" + this.totalResponseTime.getVariance() + ")"
          ;
    }

  }

  /**
   * @param globalSimulationTime
   */
  public JSimQueueFactory(final JSimTime globalSimulationTime) {
    this.globalTime = globalSimulationTime;
  }

  public JSimQueue lookup(final String queueName) {
    return this.queueCache.computeIfAbsent(queueName, unused -> {
      if (queueName.equals("/dev/null")) {
        return new SinkHole();
      }

      return new JSimQueueImpl(queueName);
    });
  }

  /**
   * @param simulationId
   * @return
   */
  public long getCountOfQueues(final UUID simulationId) {
    return this.queueCache.keySet().stream()
        .filter(key -> key.contains(simulationId.toString()))
        .count();
  }

  /**
   * @return
   */
  public Collection<JSimQueue> getQueues() {
    return this.queueCache.values();
  }

}
