package cz.zcu.fav.kiv.jsim.link;

import java.util.UUID;

import cz.zcu.fav.kiv.jsim.core.JSimSimulation;

/**
 * JSimSmulationFactory.java
 *    6. 11. 2015
 * @author Lukáš Kvídera
 * @version 0.0
 */
public final class JSimSmulationFactory {

  public static JSimSimulation createNewInstance(final String name) {
    return new JSimSimulation(name, UUID.randomUUID());
  }
}
