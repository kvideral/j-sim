package cz.zcu.fav.kiv.jsim.link;

import java.util.Collection;
import java.util.HashMap;
import java.util.Map;
import java.util.UUID;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * JSimFlowMeasurePointFactory.java
 *    10. 12. 2015
 * @author Lukáš Kvídera
 * @version 1.0
 */
public final class JSimFlowMeasurePointFactory {

  private static final Logger LOG = LoggerFactory.getLogger(JSimFlowMeasurePointFactory.class);

  private static final Map<String, JSimFlowMeasurePoint> cache = new HashMap<>();

  private JSimFlowMeasurePointFactory() { /* hidden */ }

  public static JSimFlowMeasurePoint createMeasurePoint(final UUID simulationId, final String pointName, final String queueName) {
    return cache.computeIfAbsent(
        simulationId + queueName,
        unused -> new JSimFlowMeasurePoint(simulationId, pointName, queueName)) ;
  }

  /**
   * @return
   */
  public static Collection<JSimFlowMeasurePoint> getMeasurePoints() {
    return cache.values();
  }
}
