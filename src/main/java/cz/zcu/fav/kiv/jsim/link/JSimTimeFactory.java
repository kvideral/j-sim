package cz.zcu.fav.kiv.jsim.link;

import java.util.UUID;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentMap;

/**
 * JSimTimeFactory.java
 *    31. 10. 2015
 * @author Lukáš Kvídera
 * @version 1.0
 */
public class JSimTimeFactory {

  /**
   * JSimTime.java
   *    31. 10. 2015
   *
   * This class represents virtual discrete time in simulation.
   * Usually single instance is shared between all competing processes.
   *
   * @author Lukáš Kvídera
   * @version 0.0
   */
  public static class JSimTime {
    private double time;

    /**
     * Creates zero initialized time instance.
     */
    public JSimTime() { }

    /**
     * @param Initialization time.
     */
    public JSimTime(final double time) {
      this.time = time;
    }

    @Override
    public String toString() {
      return String.valueOf(this.time);
    }


    /**
     * @return the time
     */
    public double getTime() {
      return this.time;
    }

    /**
     * @param time the time to set
     */
    public void setTime(final double time) {
      this.time = time;
    }

    /**
     * Creates instance of simulation time at given point.
     * @param time to set
     * @return Simulation time
     */
    public static JSimTime valueOf(final double time) {
      return new JSimTime(time);
    }

    /**
     * @param Creates instance of simulation time at given point.
     * @return
     */
    public static JSimTime valueOf(final JSimTime creationTime) {
      return new JSimTime(creationTime.getTime());
    }

    /**
     * Compares this and other simulation time.
     *
     * If this is before other, then returns true.
     * @param other other time to compare
     * @return true if this is before
     */
    public boolean isBefore(final JSimTime other) {
      return this.time < other.time;
    }

    /**
     * Compares this and other simulation time.
     *
     * @param other other time to compare
     * @return true if this is after
     */
    public boolean isAfter(final JSimTime other) {
      return this.time > other.time;
    }
  }

  private static final ConcurrentMap<UUID, JSimTime> timeMap = new ConcurrentHashMap<>();

  /**
   * Retrieves global time from given name.
   * @param simulationId name of global time
   * @return global time
   */
  public static JSimTime getGlobalTime(final UUID simulationId) {
    return timeMap.computeIfAbsent(simulationId, unused -> new JSimTime());
  }

}
