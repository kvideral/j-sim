package cz.zcu.fav.kiv.jsim.process;

import javax.annotation.PostConstruct;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import cz.zcu.fav.kiv.jsim.core.JSimProcessImpl;
import cz.zcu.fav.kiv.jsim.core.iface.JSimQueue;
import cz.zcu.fav.kiv.jsim.link.JSimObject;
import cz.zcu.fav.kiv.jsim.link.JSimQueueFactory;
import cz.zcu.fav.kiv.jsim.link.JSimQueueFactoryManager;
import cz.zcu.fav.kiv.jsim.random.JSimDistribution;

/**
 * ProcessWithInputQueue.java
 *    6. 11. 2015
 * @author Lukáš Kvídera
 * @version 1.0
 */
public class ConsumerProcess extends JSimProcessImpl {

  private static final Logger LOG = LoggerFactory.getLogger(ConsumerProcess.class);
  private JSimQueue inputQueue;

  public ConsumerProcess(final String name, final JSimDistribution distribution) {
    super(name, distribution);
  }

  @PostConstruct
  public void init() {
    LOG.trace("init()");
    final JSimQueueFactory instance = JSimQueueFactoryManager.getInstance(this.getSimulationId());
    this.inputQueue = instance.lookup("Consumers input queue");
  }

  @Override
  protected JSimObject serve() {
    LOG.trace("call() It's {} o'clock", this.getSimulationTime());
    return this.inputQueue.get();
  }

  @Override
  public void doWork(final JSimObject o) {
    LOG.trace("doWork(o={})", o);
  }

}
