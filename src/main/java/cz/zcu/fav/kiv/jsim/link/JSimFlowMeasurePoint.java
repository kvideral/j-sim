package cz.zcu.fav.kiv.jsim.link;

import java.util.UUID;
import java.util.function.Consumer;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import cz.zcu.fav.kiv.jsim.link.JSimTimeFactory.JSimTime;

/**
 * JSimFlowMeasurePoint.java
 *    10. 12. 2015
 * @author Lukáš Kvídera
 * @version 1.0
 * @since 1.0
 */
public class JSimFlowMeasurePoint implements Consumer<JSimObject> {

  private static final Logger LOG = LoggerFactory.getLogger(JSimFlowMeasurePoint.class);

  private final JSimStatistics statistics = new JSimStatistics();
  private final Consumer<JSimObject> queue;
  private final JSimTime globalTime;
  private final String name;
  private JSimTime lastRequest;


  /**
   * @param simulationId
   * @param queueName
   * @param queueName2
   */
  public JSimFlowMeasurePoint(final UUID simulationId, final String pointName, final String queueName) {
    final JSimQueueFactory factory = JSimQueueFactoryManager.getInstance(simulationId);
    this.queue = factory.lookup(queueName);
    this.globalTime = JSimTimeFactory.getGlobalTime(simulationId);
    this.lastRequest = JSimTime.valueOf(0);
    this.name = pointName;
  }

  @Override
  public void accept(final JSimObject t) {
    LOG.trace("accept(t={})", t);

    final double diff = this.globalTime.getTime() - this.lastRequest.getTime();
    this.statistics.accept(diff);

    this.lastRequest = JSimTime.valueOf(this.globalTime.getTime());
    this.queue.accept(t);
  }

  public double getMean() {
    return this.statistics.getMean();
  }

  public double getVariance() {
    return this.statistics.getVariance();
  }

  @Override
  public String toString() {
    return this.name
        + "\n\tmean=" + this.getMean()
        + "\n\tvar=" + this.getVariance()
        ;
  }


}
