package cz.zcu.fav.kiv.jsim.link;

import java.util.Objects;
import java.util.Random;
import java.util.UUID;
import java.util.function.Consumer;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import cz.zcu.fav.kiv.jsim.link.JSimTimeFactory.JSimTime;

/**
 * JSimProbabilitySplitter.java
 *    6. 12. 2015
 * @author Lukáš Kvídera
 * @version 1.0
 * @since 1.0
 */
public class JSimProbabilitySplitter implements Consumer<JSimObject> {

  private static final Logger LOG = LoggerFactory.getLogger(JSimProbabilitySplitter.class);

  private final Random rnd = new Random(0);
  private final JSimTime globalTime;
  private final Consumer<JSimObject> outputQueue1;
  private final Consumer<JSimObject> outputQueue2;
  private final double probability;

  public JSimProbabilitySplitter(
      final double probability,
      final UUID simulationId,
      final String firstQueueName,
      final String secondQueueName
      ) {
    this.probability = probability;
    this.globalTime = JSimTimeFactory.getGlobalTime(
        Objects.requireNonNull(simulationId, "Simulation id cannot be null!")
        );

    final JSimQueueFactory factory = JSimQueueFactoryManager.getInstance(simulationId);
    this.outputQueue1 = factory.lookup(Objects.requireNonNull(firstQueueName, "Queue name cannot be null!"));
    this.outputQueue2 = factory.lookup(Objects.requireNonNull(secondQueueName, "Queue name cannot be null!"));
  }

  @Override
  public void accept(final JSimObject t) {
    LOG.trace("accept(t={}) gt={}", t, this.globalTime);
    if (this.rnd.nextDouble() < this.probability) {
      this.outputQueue1.accept(t);
    } else {
      this.outputQueue2.accept(t);
    }
  }

}
