package cz.zcu.fav.kiv.jsim.core;

import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentMap;

import cz.zcu.fav.kiv.jsim.ipc.JSimMessageBox;

/**
 * JSimMessageService.java
 *    31. 10. 2015
 * @author Lukáš Kvídera
 * @version 0.0
 */
public class JSimMessageService {

  private static ConcurrentMap<JSimProcess, JSimMessageBox> mBoxes = new ConcurrentHashMap<>();

  /**
   * @param receiver
   * @return
   */
  public static JSimMessageBox getMessageBox(final JSimProcess receiver) {
    JSimMessageBox mBox = mBoxes.get(receiver);
    if (mBox == null) {
      mBox = new JSimMessageBox(receiver.toString());
      mBoxes.put(receiver, mBox);
    }
    return mBox;
  }
}
