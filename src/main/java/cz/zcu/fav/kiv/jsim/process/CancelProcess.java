package cz.zcu.fav.kiv.jsim.process;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.Optional;

import cz.zcu.fav.kiv.jsim.core.JSimCalendarEvent;
import cz.zcu.fav.kiv.jsim.core.JSimProcessImpl;
import cz.zcu.fav.kiv.jsim.core.JSimScheduler;
import cz.zcu.fav.kiv.jsim.core.exception.JSimSimulationAlreadyTerminatedException;
import cz.zcu.fav.kiv.jsim.core.exception.JSimTooManyProcessesException;
import cz.zcu.fav.kiv.jsim.link.JSimObject;
import cz.zcu.fav.kiv.jsim.random.JSimConstantDistribution;

/**
 * A simple process example. It tries to passivate another process but is not successful.
 */
public class CancelProcess extends JSimProcessImpl
{
  private final CancelProcess procToCancel;
  public boolean fired;

  private class CancelScheduler implements JSimScheduler {

    /**
     *
     */
    public CancelScheduler() {

    }

    @Override
    public List<JSimCalendarEvent> getSchedule() {
      final List<JSimCalendarEvent> schedule = CancelProcess.this.fired ? Collections.emptyList()
          : Arrays.asList(new JSimCalendarEvent(
              CancelProcess.this.distribution.next(),
              CancelProcess.this));
      CancelProcess.this.fired = true;
      return schedule;
    }

    @Override
    public List<JSimCalendarEvent> getJobsToUnschedule() {
      return Arrays.asList(new JSimCalendarEvent(0.0, CancelProcess.this.getProcToCancel()));
    }

  }

  private class ConstantScheduler implements JSimScheduler {

    /**
     *
     */
    public ConstantScheduler() {
      // TODO Auto-generated constructor stub
    }

    /* (non-Javadoc)
     * @see cz.zcu.fav.kiv.jsim.core.JSimScheduler#getSchedule()
     */
    @Override
    public List<JSimCalendarEvent> getSchedule() {
      return Arrays.asList(
          new JSimCalendarEvent(
              CancelProcess.this.getSimulationTime().getTime() + CancelProcess.this.distribution.next(),
              CancelProcess.this));
    }

    /* (non-Javadoc)
     * @see cz.zcu.fav.kiv.jsim.core.JSimScheduler#getJobsToUnschedule()
     */
    @Override
    public List<JSimCalendarEvent> getJobsToUnschedule() {
      return Collections.emptyList();
    }

  }

  /**
   * Note that you must ALWAYS write your own constructor &ndash; JSimProcess' one has two parameters.
   * @throws JSimTooManyProcessesException
   * @throws JSimSimulationAlreadyTerminatedException
   */
  public CancelProcess(final String name, final double interval, final CancelProcess process1) throws JSimSimulationAlreadyTerminatedException, JSimTooManyProcessesException
  {
    super(name, new JSimConstantDistribution(interval));
    this.procToCancel = process1;
  } // constructor

  @Override
  protected JSimObject serve() {
    // Are we good or bad?
    if (this.getProcToCancel() != null){
      this.message("It is " + this.getSimulationTime() + " o'clock.");
      this.message(this.getName() + ": Hello! I'm going to cancel " + this.getProcToCancel().getName() + "!");
      this.message(this.getName() + ": It's done. It will not wake up anymore. Bye.");
    } else {
      this.message(this.getName() + ": Hello! It's " + this.getSimulationTime() + " o'clock.");
      this.message(this.getName() + ": I'm still here. See you in 0.45 sec.");
      this.message(this.getName() + ": Good-bye.");
    } // else
    return null;
  }

  /**
   * This is the life of the process. We will try to passivate the other guy or print hello.
   * @return
   */
  @Override
  public void doWork(final JSimObject jSimObject) {
    /* nothing to do */
  } // life

  @Override
  public Optional<JSimScheduler> getScheduler() {
    return this.getProcToCancel() == null
        ? Optional.of(new ConstantScheduler() )
            : Optional.of(new CancelScheduler());
  }

  /**
   * @return the procToCancel
   */
  public CancelProcess getProcToCancel() {
    return this.procToCancel;
  }

} // class CancelProcess

