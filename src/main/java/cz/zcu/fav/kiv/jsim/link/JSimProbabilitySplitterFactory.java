package cz.zcu.fav.kiv.jsim.link;

import java.util.HashMap;
import java.util.Map;
import java.util.UUID;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * JSimProbabilitySplitterFactory.java
 *    6. 12. 2015
 * @author Lukáš Kvídera
 * @version 0.0
 */
public final class JSimProbabilitySplitterFactory {

  private static final Logger LOG = LoggerFactory.getLogger(JSimProbabilitySplitterFactory.class);

  private static final Map<String, JSimProbabilitySplitter> cache = new HashMap<>();

  private JSimProbabilitySplitterFactory() { /* empty */ }

  public static JSimProbabilitySplitter createSplitter(
      final double probability,
      final UUID simulationId,
      final String firstQueueName,
      final String secondQueueName
      ) {
    LOG.trace("createSplitter(probability={}, simulationId={}, firstQueueName={}, secondQueueName={})",
        probability, simulationId, firstQueueName, secondQueueName);
    return cache.computeIfAbsent(
        simulationId + firstQueueName + secondQueueName,
        unused -> new JSimProbabilitySplitter(probability, simulationId, firstQueueName, secondQueueName)
        );
  }

}
