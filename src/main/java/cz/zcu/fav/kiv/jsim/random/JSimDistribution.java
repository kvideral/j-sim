package cz.zcu.fav.kiv.jsim.random;


/**
 * Distribution.java
 *    11. 12. 2015
 * @author Lukáš Kvídera
 * @version 1.0
 * @since 1.0
 */
public interface JSimDistribution {

  double next();
}
