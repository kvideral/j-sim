package cz.zcu.fav.kiv.jsim.process;

import java.util.function.Consumer;

import javax.annotation.PostConstruct;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import cz.zcu.fav.kiv.jsim.core.JSimProcessImpl;
import cz.zcu.fav.kiv.jsim.core.exception.JSimSimulationAlreadyTerminatedException;
import cz.zcu.fav.kiv.jsim.core.exception.JSimTooManyProcessesException;
import cz.zcu.fav.kiv.jsim.link.JSimObject;
import cz.zcu.fav.kiv.jsim.link.JSimQueueFactory;
import cz.zcu.fav.kiv.jsim.link.JSimQueueFactoryManager;
import cz.zcu.fav.kiv.jsim.random.JSimDistribution;

/**
 * WebUserGenerator.java
 *    6. 12. 2015
 * @author Lukáš Kvídera
 * @version 0.0
 */
public class WebUserGenerator extends JSimProcessImpl {

  private static final Logger LOG = LoggerFactory.getLogger(WebUserGenerator.class);

  private Consumer<JSimObject> mainInputQueue;

  /**
   * @param name
   * @throws JSimSimulationAlreadyTerminatedException
   * @throws JSimTooManyProcessesException
   */
  public WebUserGenerator(final String name, final JSimDistribution distribution) throws JSimSimulationAlreadyTerminatedException, JSimTooManyProcessesException {
    super(name, distribution);
  }

  @PostConstruct
  public void init() {
    final JSimQueueFactory factory = JSimQueueFactoryManager.getInstance(this.getSimulationId());
    this.mainInputQueue = factory.lookup("mainInputQueue");
  }

  @Override
  protected JSimObject serve() {
    return new JSimObject(this.getSimulationTime());
  }

  @Override
  public void doWork(final JSimObject jSimObject) {
    LOG.trace("doWork(jSimObject={})", jSimObject);
    this.mainInputQueue.accept(jSimObject);
  }

}
