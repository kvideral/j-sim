package cz.zcu.fav.kiv.jsim.process;

import java.util.function.Consumer;
import java.util.function.Supplier;

import javax.annotation.PostConstruct;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import cz.zcu.fav.kiv.jsim.core.JSimProcessImpl;
import cz.zcu.fav.kiv.jsim.link.JSimObject;
import cz.zcu.fav.kiv.jsim.link.JSimProbabilitySplitterFactory;
import cz.zcu.fav.kiv.jsim.link.JSimQueueFactory;
import cz.zcu.fav.kiv.jsim.link.JSimQueueFactoryManager;
import cz.zcu.fav.kiv.jsim.random.JSimDistribution;


/**
 * LoadBalancer.java
 *    6. 12. 2015
 * @author Lukáš Kvídera
 * @version 1.0
 */
public class LoadBalancer extends JSimProcessImpl {

  private static final Logger LOG = LoggerFactory.getLogger(LoadBalancer.class);

  private Supplier<JSimObject> inputQueue;
  private Consumer<JSimObject> loginPageSplitter;

  public LoadBalancer(final String name, final JSimDistribution distibution) {
    super(name, distibution);
  }

  @PostConstruct
  public void init() {
    LOG.trace("init()");
    this.loginPageSplitter = JSimProbabilitySplitterFactory.createSplitter(
        0.5,
        this.getSimulationId(),
        "loginQueue",
        "pageQueue"
        );

    final JSimQueueFactory factory = JSimQueueFactoryManager.getInstance(this.getSimulationId());
    this.inputQueue = factory.lookup("mainInputQueue");
  }

  @Override
  protected JSimObject serve() {
    return this.inputQueue.get();
  }

  @Override
  protected void doWork(final JSimObject jSimObject) {
    LOG.trace("doWork(jSimObject={})", jSimObject);
    this.loginPageSplitter.accept(jSimObject);
  }

}
