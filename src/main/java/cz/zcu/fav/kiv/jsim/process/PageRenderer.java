package cz.zcu.fav.kiv.jsim.process;

import java.util.function.Consumer;
import java.util.function.Supplier;

import javax.annotation.PostConstruct;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import cz.zcu.fav.kiv.jsim.core.JSimProcessImpl;
import cz.zcu.fav.kiv.jsim.link.JSimObject;
import cz.zcu.fav.kiv.jsim.link.JSimProbabilitySplitterFactory;
import cz.zcu.fav.kiv.jsim.link.JSimQueueFactory;
import cz.zcu.fav.kiv.jsim.link.JSimQueueFactoryManager;
import cz.zcu.fav.kiv.jsim.random.JSimDistribution;


/**
 * LoadBalancer.java
 *    6. 12. 2015
 * @author Lukáš Kvídera
 * @version 0.0
 */
public class PageRenderer extends JSimProcessImpl {

  private static final Logger LOG = LoggerFactory.getLogger(PageRenderer.class);

  private Supplier<JSimObject> pageQueue;
  private Consumer<JSimObject> databaseExitSplitter;

  public PageRenderer(final String name, final JSimDistribution distribution) {
    super(name, distribution);
  }

  @PostConstruct
  public void init() {
    LOG.trace("init()");
    this.databaseExitSplitter = JSimProbabilitySplitterFactory.createSplitter(
        0.2,
        this.getSimulationId(),
        "databaseQueue",
        "/dev/null"
        );

    final JSimQueueFactory factory = JSimQueueFactoryManager.getInstance(this.getSimulationId());
    this.pageQueue = factory.lookup("pageQueue");
  }

  @Override
  protected JSimObject serve() {
    return this.pageQueue.get();
  }

  @Override
  public void doWork(final JSimObject jSimObject) {
    LOG.trace("doWork(jSimObject={})", jSimObject);
    this.databaseExitSplitter.accept(jSimObject);
  }

}
