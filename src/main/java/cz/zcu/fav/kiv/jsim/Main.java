package cz.zcu.fav.kiv.jsim;

import java.io.IOException;

import cz.zcu.fav.kiv.jsim.core.JSimProcessImpl;
import cz.zcu.fav.kiv.jsim.core.JSimSimulation;
import cz.zcu.fav.kiv.jsim.core.exception.JSimMethodNotSupportedException;
import cz.zcu.fav.kiv.jsim.core.exception.JSimSimulationAlreadyTerminatedException;
import cz.zcu.fav.kiv.jsim.core.exception.JSimTooManyProcessesException;
import cz.zcu.fav.kiv.jsim.link.JSimFlowMeasurePointFactory;
import cz.zcu.fav.kiv.jsim.link.JSimQueueFactory;
import cz.zcu.fav.kiv.jsim.link.JSimQueueFactoryManager;
import cz.zcu.fav.kiv.jsim.link.JSimSmulationFactory;
import cz.zcu.fav.kiv.jsim.process.CancelProcess;
import cz.zcu.fav.kiv.jsim.process.ConsumerProcess;
import cz.zcu.fav.kiv.jsim.process.DatabaseCenter;
import cz.zcu.fav.kiv.jsim.process.LoadBalancer;
import cz.zcu.fav.kiv.jsim.process.LoginProcessor;
import cz.zcu.fav.kiv.jsim.process.PageRenderer;
import cz.zcu.fav.kiv.jsim.process.ProducerProcess;
import cz.zcu.fav.kiv.jsim.process.WebUserGenerator;
import cz.zcu.fav.kiv.jsim.random.JSimExponentialStream;
import cz.zcu.fav.kiv.jsim.random.JSimGaussianStream;
import joptsimple.OptionParser;
import joptsimple.OptionSet;

/**
 * Main.java
 *    28. 10. 2015
 * @author Lukáš Kvídera
 * @version 1.0
 */
public final class Main {

  private static void expSimulation(final Integer iterations) {
    final JSimSimulation simulation = JSimSmulationFactory.createNewInstance("Allowed Cancel Simulation");
    try {
      final JSimProcessImpl process1 = new CancelProcess("Guy No 1", 0.45, null);
      simulation.addProcess(process1);
      simulation.addProcess(new CancelProcess("Guy No 2", 2.0, (CancelProcess) process1));
      simulation.addProcess(new ProducerProcess("Nějaký ten producer", new JSimExponentialStream(1.0)));
      simulation.addProcess(new ConsumerProcess("Nějaký ten konzument", new JSimExponentialStream(1.0)));
      simulation.addProcess(new LoadBalancer("Hlavní load balancer", new JSimExponentialStream(20.0)));
      simulation.addProcess(new LoginProcessor("Zpracovávač přihlášení", new JSimExponentialStream(10.0)));
      simulation.addProcess(new PageRenderer("Vykreslovač stránek", new JSimExponentialStream(15.0)));
      simulation.addProcess(new DatabaseCenter("Databázové centrum", new JSimExponentialStream(8.0)));
      simulation.addProcess(new WebUserGenerator("Příval uživatelů", new JSimExponentialStream(10.1)));
      for (int i = 0; (i < iterations.intValue()) && (simulation.step() == true); i++) {
        /* do nothing */
      }
    } catch (JSimSimulationAlreadyTerminatedException | JSimTooManyProcessesException | JSimMethodNotSupportedException e) {
      e.printStackTrace();
    } finally {
      simulation.shutdown();
      simulation.printStatistics();
      final JSimQueueFactory instance = JSimQueueFactoryManager.getInstance(simulation.getSimulationId());
      instance.getQueues().forEach(queue -> System.out.println(queue));

      JSimFlowMeasurePointFactory.getMeasurePoints().forEach(point -> System.out.println(point));
    }
  }

  private static void gaussSimulation(final Integer iterations) {
    final double[] variances = {0.05, 0.2, 0.7};
    for (final double variance : variances) {
      final JSimSimulation simulation = JSimSmulationFactory.createNewInstance("Allowed Cancel Simulation");
      try {
//        final JSimProcessImpl process1 = new CancelProcess("Guy No 1", 0.45, null);
//        simulation.addProcess(process1);
//        simulation.addProcess(new CancelProcess("Guy No 2", null, (CancelProcess) process1));
//        simulation.addProcess(new ProducerProcess("Nějaký ten producer", new JSimExponentialStream(1.0)));
//        simulation.addProcess(new ConsumerProcess("Nějaký ten producer", new JSimExponentialStream(1.0)));
        simulation.addProcess(new LoadBalancer("Hlavní load balancer", new JSimExponentialStream(35.0)));
        simulation.addProcess(new LoginProcessor("Zpracovávač přihlášení", new JSimExponentialStream(15.0)));
        simulation.addProcess(new PageRenderer("Vykreslovač stránek", new JSimExponentialStream(30.0)));
        simulation.addProcess(new DatabaseCenter("Databázové centrum", new JSimExponentialStream(25.0)));
        simulation.addProcess(new WebUserGenerator("Příval uživatelů", new JSimGaussianStream(16.0, variance)));
        for (int i = 0; (i < iterations.intValue()) && (simulation.step() == true); i++) {
          /* do nothing */
        }
      } catch (JSimSimulationAlreadyTerminatedException | JSimTooManyProcessesException | JSimMethodNotSupportedException e) {
        e.printStackTrace();
      } finally {
        simulation.shutdown();
        final JSimQueueFactory instance = JSimQueueFactoryManager.getInstance(simulation.getSimulationId());
        instance.getQueues().forEach(queue -> System.out.println(queue));

        JSimFlowMeasurePointFactory.getMeasurePoints().forEach(point -> System.out.println(point));
      }
    }
  }

  public static void main(final String[] args) throws IOException  {
    final OptionParser parser = new OptionParser();
    parser.allowsUnrecognizedOptions();
    parser.accepts("i").withRequiredArg().ofType(Integer.class).defaultsTo(10000000);
    parser.accepts("EXP", "Exponential distribution");
    parser.accepts("GAUSS", "Gaussian distribution");

    final OptionSet options = parser.parse(args);
    final Integer iterations = (Integer) options.valueOf("i");

    if (!options.has("EXP") && !options.has("GAUSS")) {
      parser.printHelpOn(System.err);
      System.exit(1);
    }

    expSimulation(iterations);
    // gaussSimulation(iterations);
  } // main
}
