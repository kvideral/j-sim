package cz.zcu.fav.kiv.jsim.core;

import java.util.List;

/**
 * JSimScheduler.java
 *    30. 10. 2015
 * @author Lukáš Kvídera
 * @version 1.0
 */
public interface JSimScheduler {

  /**
   * Retrieves list of events, that will be scheduled.
   * List can be empty.
   *
   * @return events to be scheduled
   */
  List<JSimCalendarEvent> getSchedule();

  /**
   * Retrieves list of events, that will be unscheduled.
   * List can be empty.
   *
   * @return events to be unscheduled
   */
  List<JSimCalendarEvent> getJobsToUnschedule();

}
