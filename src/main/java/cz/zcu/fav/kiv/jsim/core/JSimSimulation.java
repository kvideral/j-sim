/*
 *  Copyright (c) 2000-2006 Jaroslav Kačer <jaroslav@kacer.biz>
 *  Copyright (c) 2003 Pavel Domecký
 *  Copyright (c) 2004 University of West Bohemia, Pilsen, Czech Republic
 *  Licensed under the Academic Free License version 2.1
 *  J-Sim source code can be downloaded from http://www.j-sim.zcu.cz/
 *
 */

package cz.zcu.fav.kiv.jsim.core;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.List;
import java.util.Objects;
import java.util.Optional;
import java.util.SortedSet;
import java.util.TreeSet;
import java.util.UUID;

import javax.annotation.PostConstruct;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import cz.zcu.fav.kiv.jsim.core.exception.JSimException;
import cz.zcu.fav.kiv.jsim.core.exception.JSimInvalidParametersException;
import cz.zcu.fav.kiv.jsim.core.exception.JSimInvalidProcessStateException;
import cz.zcu.fav.kiv.jsim.core.exception.JSimKernelPanicException;
import cz.zcu.fav.kiv.jsim.core.exception.JSimMethodNotSupportedException;
import cz.zcu.fav.kiv.jsim.gui.JSimChange;
import cz.zcu.fav.kiv.jsim.gui.JSimDisplayable;
import cz.zcu.fav.kiv.jsim.gui.JSimMainWindow;
import cz.zcu.fav.kiv.jsim.ipc.JSimSemaphore;
import cz.zcu.fav.kiv.jsim.link.JSimQueueFactory;
import cz.zcu.fav.kiv.jsim.link.JSimQueueFactoryManager;
import cz.zcu.fav.kiv.jsim.link.JSimTimeFactory;
import cz.zcu.fav.kiv.jsim.link.JSimTimeFactory.JSimTime;

/**
 * The JSimSimulation class instances represent theoretical simulation models, containing processes, queues, and other elements.
 *
 * Every process and queue must have a simulation specified as a constructor parameter, otherwise the creation will not be successful. To
 * the user, the JSimSimulation class provides the step() method &ndash; execution of one simulation step. Alternatively, the runGUI()
 * method can be used, causing the simulation to run in graphic mode.
 *
 * @author Jarda KAČER
 * @author Pavel DOMECKÝ
 *
 * @version J-Sim version 0.6.0
 *
 * @since J-Sim version 0.1.0
 */
public class JSimSimulation
{
  // Importance of printed strings
  public static final int PRN_MESSAGE = 0;
  public static final int PRN_ERROR = 1;

  // Error constans
  public static final long NEW_PROCESS_FORBIDDEN = -1;
  public static final long NEW_QUEUE_FORBIDDEN = -1;
  public static final long NEW_SEMAPHORE_FORBIDDEN = -1;

  // ------------------------------------------------------------------------------------------------------------------------------------

  /**
   * Common logger for all instances of this class. By default, all logging information goes to a file. Only severe events go to the
   * console, in addition to a file.
   */
  private static final Logger LOG = LoggerFactory.getLogger(JSimSimulation.class);

  /**
   * The simulation's number. These numbers are unique for a given JVM instance.
   */
  private final UUID simulationId;

  /**
   * The simulation's name.
   */
  private final String myName;

  /**
   * The simulation's mode &ndash; text, GUI batch, GUI interactive.
   */
  protected final JSimSimulationMode mode;

  /**
   * The currently running process. At most one process can run at the same time.
   */
  protected JSimProcess runningProcess;

  /**
   * The simulation's calendar of events.
   */
  private final JSimCalendar calendar;

  /**
   * The simulation's current time.
   */
  private final JSimTime time;

  /**
   * The simulation's state &ndash; not started, in progress, terminated.
   */
  private JSimSimulationState simulationState = JSimSimulationState.NOT_STARTED;

  /**
   * All processes of the simulation.
   */
  private final SortedSet<JSimProcess> processes = new TreeSet<>();

  /**
   * All processes that have ever existed in the simulation. The list of processes in the GUI window shows even processes that have
   * terminated already.
   */
  private final SortedSet<JSimProcess> processesForGUI = new TreeSet<>();

  /**
   * All queues of the simulation.
   */
  private final JSimQueueFactory queueFactory;


  /**
   * All semaphores of the simulation.
   */
  private final SortedSet<JSimSemaphore> semaphores = new TreeSet<>();

  /**
   * Lock used when switching between the simulation and its main GUI window.
   */
  private final Object graphicLock = new Object();

  /**
   * Lock used to synchronize (possible) concurrent calls to step(). The implicit lock cannot be used since processes use callbacks to the
   * simulation's synchronized methods, e.g. getRunningProcess() in getReady(), and the lock for synchonizing step() must not be released
   * when switching to a process's thread from the main thread.
   */
  protected final Object stepLock = new Object();

  /**
   * Lock used when the simulation waits in waitForWindowClosure(). This is only possible when the simulation runs in GUI batch mode.
   */
  private final Object waitForWindowClosureLock = new Object();

  /**
   * The simulation's main window.
   */
  protected JSimMainWindow mainWindow;

  /**
   * Flag saying that the main window is open.
   */
  protected boolean windowOpen;

  /**
   * Flag saying that the simulation is just now waiting in waitForWindowClosure().
   */
  private boolean waitingForWindowClosure;

  /**
   * Flag saying that the first step of simulation has been executed. This flag is important in GUI batch mode where the simulation itself
   * takes care about the GUI main window.
   */
  protected boolean firstStepExecuted;

  /**
   * Flag indicating that the main window has already been opened (and maybe closed). The flag is relevant in batch GUI mode only where it
   * is necessary to distinguish whether messages should go to the main window or to the console. If an output/step request is received,
   * the main window must be open, but only if it has not been opened/closed yet.
   */
  protected boolean mainWindowHasAlreadyExisted;

  /**
   * Observable object notifying the GUI about possible changes.
   */
  protected JSimChange guiUpdate = new JSimChange();

  /**
   * GUI update delta. This is a simulation time interval between two consecutive updates of GUI output. Its value does not change during
   * simulation execution. If it is zero, the output is updated after every simulation step. Otherwise, the output is updated only if the
   * difference between the current simulation time and lastGUIUpdate is equal to or greater than this delta.
   */
  protected double nextGUIUpdateDelta;

  /**
   * Last GUI update time.
   */
  protected double lastGUIUpdate;


  // ------------------------------------------------------------------------------------------------------------------------------------

  /**
   * Creates a new simulation with no processes, no queues and no graphic window. The simulation will run in text (console) mode and no
   * graphic output will be allowed.
   *
   * @param name
   *            Name of the simulation.
   *
   * @exception JSimInvalidParametersException
   *                This exception is thrown out if the specified name is is null.
   */
  public JSimSimulation(final String name, final UUID uuid) throws JSimInvalidParametersException
  {
    this(name, JSimSimulationMode.TEXT, 0.0, uuid);
  } // constructor

  /**
   * Creates a new simulation with no processes, no queues and no graphic window. According to the simulation mode specified as a
   * parameter, it will / will not be allowed to use graphic output later. If GUI batch or interactive mode is used, the output will be
   * updated after every step.
   *
   * @param name
   *            Name of the simulation.
   * @param mode
   *            Simulation mode.
   *
   * @exception JSimInvalidParametersException
   *                This exception is thrown out if the specified name is is null or an invalid simulation mode is given to the
   *                constructor.
   */
  public JSimSimulation(final String name, final JSimSimulationMode mode, final UUID uuid) throws JSimInvalidParametersException
  {
    this(name, mode, 0.0, uuid);
  } // constructor

  /**
   * Creates a new simulation with no processes, no queues and no graphic window. According to the simulation mode specified as a
   * parameter, it will / will not be allowed to use graphic output later.
   *
   * @param name
   *            Name of the simulation.
   * @param mode
   *            Simulation mode.
   * @param guiUpdateDelta
   *            A simulation time interval after which the GUI output will be updated. If set to zero, the output will be updated after
   *            every simulation step. This may be time consuming and inappropriate for some situations.
   *
   * @exception JSimInvalidParametersException
   *                This exception is thrown out if the specified name is is null, an invalid simulation mode is given to the constructor,
   *                or if the update interval is less than zero.
   */
  public JSimSimulation(
      final String name,
      final JSimSimulationMode mode,
      final double guiUpdateDelta,
      final UUID uuid
      )
  {
    if (guiUpdateDelta < 0.0) {
      throw new JSimInvalidParametersException("JSimSimulation.JSimSimulation(): guiUpdateDelta");
    }

    this.myName = Objects.requireNonNull(name, "JSimSimulation.JSimSimulation(): name");
    this.mode = mode;

    this.simulationId = uuid;

    this.calendar = new JSimCalendar();
    this.time = JSimTimeFactory.getGlobalTime(this.simulationId);
    this.queueFactory = JSimQueueFactoryManager.createNewFactory(this.simulationId, this.time);
    this.nextGUIUpdateDelta = guiUpdateDelta;
//    uuid.forEach(
//        task ->  {
//          task.setSimulationId(this.simulationId);
//          task.setTime(this.time);
//          task.getScheduler().get().getSchedule().forEach(
//              event -> this.calendar.addWholeEvent(event)
//              );
//          try {
//            task.setProcessState(JSimProcessState.SCHEDULED);
//          } catch (final Exception e) {
//            LOG.error("Something bad happend!", e);
//          }
//        });

    LOG.info("A new simulation created. Name: " + this.myName + " time:" + this.time);
  } // constructor

  // ------------------------------------------------------------------------------------------------------------------------------------

  /**
   * Adds a new process to the simulation. You should never call this method. It is called automatically from JSimProcess constructor.
   *
   * @param process
   *            The process that has to be added to the simulation.
   *
   * @exception JSimInvalidParametersException
   *                This exception is thrown out if the specified process is null.
   */
  public final void addProcess(final JSimProcess process) {
    final JSimProcess processToAdd = Objects.requireNonNull(process, "JSimSimulation.addProcess(): process");
    // Welcome, process. Now we count with you.
    this.processes.add(processToAdd);
    this.processesForGUI.add(processToAdd);

    processToAdd.setSimulationId(this.simulationId);
    processToAdd.setTime(this.time);
    final Method[] declaredMethods = process.getClass().getDeclaredMethods();
    for (final Method method : declaredMethods) {
      if (method.isAnnotationPresent(PostConstruct.class)) {
        try {
          method.invoke(process);
        } catch (IllegalAccessException | IllegalArgumentException | InvocationTargetException e) {
          LOG.error("Something bad happend!", e);
        }
      }
    }


    final JSimScheduler scheduler = processToAdd.getScheduler().get();
    final List<JSimCalendarEvent> schedule = scheduler.getSchedule();

    schedule.forEach(event -> this.calendar.addWholeEvent(event));
    try {
      process.setProcessState(JSimProcessState.SCHEDULED);
    } catch (final JSimInvalidProcessStateException e) {
      e.printStackTrace();
    }
    LOG.info("Process " + processToAdd + " added to the simulation.");
  } // addProcess

  // ------------------------------------------------------------------------------------------------------------------------------------

  /**
   * Executes one simulation step. A step is a unit of activity that is terminated by a process' call to the passivate() or hold() method
   * or by a process' death. If there is no process within the simulation or no processes are scheduled or the quit button in GUI batch
   * mode is pressed, simulation will terminate and false will be returned. In all other cases, true will be returned.
   *
   * The method is synchronized with a special lock (stepLock) instead of the implicit one. The reason for not using the implicit lock is
   * that the simulation would block before entering the synchronized(processLock) section because a process would have this lock
   * (processLock) acquired while demanding to call a method synchronized with the implicit lock (e.g. getRunningProcess() in getReady())
   * and the implicit lock would never be released in step(), even not after passivation of the main thread.
   *
   * @return False if the step could not be executed because there were no events in the calendar or there were no processes in the
   *         simulation or the quit button in the GUI was pressed, true otherwise (the step was successufully completed).
   *
   * @exception JSimKernelPanicException
   *                This exception is thrown out when the state of the simulation cannot be determined or when an inconsistency of J-Sim
   *                internal structures (such as the calendar) is revealed.
   * @exception JSimMethodNotSupportedException
   *                This exception is thrown out when the simulation is running in GUI interactive mode and step() is called from
   *                elsewhere than GUI.
   */
  public boolean step() throws JSimMethodNotSupportedException
  {
    LOG.info("Step requested.");

    // Calling step() from main() is forbidden if running in GUI interactive mode
    if ((this.mode == JSimSimulationMode.GUI_INTERACTIVE) && (!this.isRunningGUI())) {
      throw new JSimMethodNotSupportedException("JSimSimulation.step(): Wrong simulation mode. You cannot call step() directly in GUI interactive mode.");
    }

    // Start the GUI if we are running in GUI batch mode and step() is called for the first time
    if ((this.mode == JSimSimulationMode.GUI_BATCH) && (!this.firstStepExecuted) && (!this.isRunningGUI()))
    {
      this.firstStepExecuted = true;
      this.runBatchGUI();
    } // if

    boolean result;

    // Some special conditions related to the GUI batch mode must be handled first.
    if (this.mode == JSimSimulationMode.GUI_BATCH)
    {
      // Pause button pressed
      while (this.mainWindow.getPausePressed())
      {
        try
        {
          LOG.info("STEP: Simulation paused by the Pause button, suspending the simulation.");
          // Waiting here until the continue button pressed
          this.stepLock.wait();
          LOG.info("STEP: The Continue button pressed, going on with the simulation.");
        } // try
        catch (final InterruptedException e)
        {
          LOG.info("STEP: Interrupted while waiting for user's permission to continue!", e);
        } // catch
      } // while getPausePressed()

      // Quit button pressed --> exit step() immediately
      if (this.mainWindow.getQuitPressed()) {
        return false;
      }
    } // if MODE_GUI_BATCH

    // We will probably wait here for a while until all new threads call their wait()
//      LOG.info("STEP: Starting new processes...");
//      if (this.startNewProcesses() == true) {
//        Thread.yield();
//      }
    LOG.info("STEP: New processes should have been started by now {}.", this.time);

    switch (this.simulationState)
    {
      // The simulation has not been started yet
      case NOT_STARTED:
        this.simulationState = JSimSimulationState.IN_PROGRESS;
      case IN_PROGRESS:
        LOG.info("STEP: Simulation in progress, trying to select a process and run it.");
        result = true;

        if (!this.calendar.isEmpty())
        {
          final JSimProcess process = this.calendar.getFirstProcess();
          this.time.setTime(this.calendar.getFirstProcessTime().getTime());
          try {
            process.call();
          } catch (final Exception e) {
            LOG.error("Something bad happend!", e);
          }
          final Optional<JSimScheduler> scheduler = process.getScheduler();
          if (scheduler.isPresent()) {
            final List<JSimCalendarEvent> schedule = scheduler.get().getSchedule();
            schedule.forEach(event -> this.calendar.addWholeEvent(event));

            final List<JSimCalendarEvent> unschedule = scheduler.get().getJobsToUnschedule();
            unschedule.forEach(event -> this.calendar.deleteEntries(event.getProcess(), true));
          }

        } else {
          LOG.info("STEP: No more scheduled processes, terminating the simulation.");
          this.simulationState = JSimSimulationState.TERMINATED;
          result = false;
        }
        // The state might have changed to TERMINATED since entering this branch of switch
        // because the calendar could be empty.
        if (this.simulationState == JSimSimulationState.IN_PROGRESS)
        {
          // Now we must prepare the calendar for the next iteration.
          // This should be done BEFORE the selected process actually runs
          // because of possible time rollbacks in InsecureSimulation.
          // If it were called after, it would remove an event added after
          // the event selected now (an event with a new simulation time
          // in the past) and this one would remain there, although it should
          // have been removed.
          // Update 0.6.0: The Insecure package has been removed but let's keep it here.
          this.calendar.jump();

        }
        break; // IN_PROGRESS
        // End of IN_PROGRESS

        // The simulation has been terminated already.
      case TERMINATED:
        LOG.warn("STEP: The simulation is already terminated, exiting.");
        result = false;
        break; // TERMINATED
        // End of TERMINATED

      default:
        LOG.error("STEP: Unexpected situation (Unknown state), J-Sim Kernel in panic. Please report.");
        throw new JSimKernelPanicException();
    } // switch simulationState

    // Now, let's update the GUI
    if ((this.windowOpen) && (this.time.getTime() >= (this.lastGUIUpdate + this.nextGUIUpdateDelta)))
    {
      this.guiUpdate.changed();
      this.lastGUIUpdate = this.time.getTime()
          ;
    } // if update necessary

    LOG.info("Step completed, return value = {}.", result);
    return result;
    // Now returning to the method that called us -- usually main().
  } // step

  /**
   * Shutdowns the simulation by interrupting all living processes. You must always call this function, preferably in the finally block at
   * the end of your main program.
   */
  public synchronized void shutdown()
  {
    LOG.info("Shutting down the simulation...");
    LOG.info("Deleting J-Sim processes...");
    LOG.info("J-Sim processes deleted.");
    this.simulationState = JSimSimulationState.TERMINATED;
    // We let all the threads finish first
    Thread.yield();

    LOG.info("Shutdown completed.");
  } // shutdown

  // ------------------------------------------------------------------------------------------------------------------------------------

  /**
   * Returns the simulation's unique number.
   *
   * @return The Simulation's unique number.
   */
  public int getSimulationNumber()
  {
    return this.simulationId.hashCode();
  } // getSimulationNumber

  /**
   * Returns the simulation's name.
   *
   * @return The simulation's name.
   */
  public String getSimulationName()
  {
    return this.myName;
  } // getSimulationName

  /**
   * Returns the number of processes present within the simulation.
   *
   * @return Number of processes present within the simulation.
   */
  public synchronized final int getNumberOfProcesses()
  {
    return this.processes.size();
  } // getNumberOfProcesses

  /**
   * Returns the number of queues present within the simulation.
   *
   * @return Number of queues present within the simulation.
   */
  public final long getNumberOfQueues()
  {
    return this.queueFactory.getCountOfQueues(this.simulationId);
  } // getNumberOfQueues

  /**
   * Returns the number of semaphores present within the simulation.
   *
   * @return Number of semaphores present within the simulation.
   */
  public synchronized final int getNumberOfSemaphores()
  {
    return this.semaphores.size();
  } // getNumberOfSemaphores

  /**
   * Returns the currently running process. If no process is running JSimCalendar.NOBODY (=null) will be returned.
   *
   * @return The currently running process or JSimCalendar.NOBODY.
   */
  public synchronized final JSimProcess getRunningProcess()
  {
    return this.runningProcess;
  } // getRunningProcess

  /**
   * Switches the execution to the main thread. You should never call this method. This method is called by a process from its hold(),
   * passivate(), or reactivate(). Calling this method does not guarantee the physical thread switch itself, it just sets up the running
   * thread variable.
   */
  protected synchronized final void switchToNobody()
  {
    this.runningProcess = JSimCalendar.NOBODY;
  } // switchToNobody

  /**
   * Returns the simulation's current time.
   *
   * @return The simulation's current time.
   */
  public synchronized final double getCurrentTime()
  {
    return this.time.getTime();
  } // getCurrentTime

  /**
   * Returns the simulation's state.
   *
   * @return The simulation's state.
   */
  public synchronized final JSimSimulationState getSimulationState()
  {
    return this.simulationState;
  } // getSimulationState

  /**
   * Returns the simulation's mode.
   *
   * @return The simulation's mode.
   */
  public synchronized final JSimSimulationMode getSimulationMode()
  {
    return this.mode;
  } // getSimulationMode

  // ------------------------------------------------------------------------------------------------------------------------------------

  /**
   * Returns the lock object used for synchronization between the simulation and its GUI window. You should never call this method.
   *
   * @return The lock object used for sychronization between the simulation and its GUI window.
   */
  public final Object getGraphicLock()
  {
    return this.graphicLock;
  } // getGraphicLock

  /**
   * Returns the lock object that protects the step() method from being invoked in parallel. It is also used for synchronization between
   * the simulation and its GUI window, while being paused. You should never call this method.
   *
   * @return The lock object that protects the step() method.
   */
  public final Object getStepLock()
  {
    return this.stepLock;
  } // getStepLock

  /**
   * Returns the lock object used for synchronization between the simulation and its GUI window, while waiting in waitForWindowClosure().
   * You should never call this method.
   *
   * @return The lock object used for synchronization between the simulation and its GUI window.
   */
  public final Object getWindowClosureLock()
  {
    return this.waitForWindowClosureLock;
  } // getEndLock

  /**
   * Returns an observable object notifying the GUI about possible changes. You should never call this method.
   *
   * @return An observable object notifying the GUI about possible changes.
   */
  public final JSimChange getChange()
  {
    return this.guiUpdate;
  } // getChange

  /**
   * Returns a set containing information about simulation elements that are to be displayed in the GUI. You should never call this
   * method.
   *
   * @return A set containing information about simulation elements.
   *
   * @param elementType
   *            Specifies the type of elements whose information should be returned. Available constants are JSimMainWindow.LIST_TYPE_*.
   *
   * @exception JSimInvalidParametersException
   *                This exception is thrown out if the elementType parameter does not specify a valid type of simulation elements.
   */
  public SortedSet<? extends JSimDisplayable> getObjectsToBeDisplayed(final int elementType) throws JSimInvalidParametersException
  {
    SortedSet<? extends JSimDisplayable> table = null;

    switch(elementType)
    {
      case JSimMainWindow.LIST_TYPE_PROCESS:
//        table = (SortedSet<? extends JSimDisplayable>) this.processesForGUI;
        break;

      case JSimMainWindow.LIST_TYPE_QUEUE:
//        table = this.queueFactory.getQueues();
        break;

      case JSimMainWindow.LIST_TYPE_SEMAPHORE:
        table = this.semaphores;
        break;

      default:
        throw new JSimInvalidParametersException("JSimSimulation: getObjectsToBeDisplayed(): elementType");
    } // switch

    return table;
  } // getObjectsToBeDisplayed

  // ------------------------------------------------------------------------------------------------------------------------------------

  /**
   * Adds a new event to the calendar, with the specified absolute time and the specified process. You should never call this method.
   * Processes themeselves use this method when they have to be scheduled.
   *
   * @param absoluteTime
   *            Absolute simulation time of the event being added to the calendar.
   * @param process
   *            Process that will be activated at the specified time.
   *
   * @exception JSimInvalidParametersException
   *                This exception is thrown out if the time or the process specified are invalid and were rejected by the calendar.
   */
  protected synchronized void addEntryToCalendar(final double absoluteTime, final JSimProcess process) throws JSimInvalidParametersException
  {
    try
    {
      this.calendar.addEntry(absoluteTime, process);
      LOG.info("A new event has been added to the calendar. (Process " + process + " at time " + absoluteTime + ").");
    } // try
    catch (final JSimInvalidParametersException e)
    {
      LOG.warn("An error occured when adding a new entry to the calendar.", e);
      throw e; // Throwing the same exception to the calling function.
    } // catch
  } // addEntryToCalendar

  /**
   * Deletes one or more entries in the calendar, concerning a process. You should never call this method.
   *
   * @param process
   *            Process whose events are to be deleted.
   * @param all
   *            Flag saying whether just one (false) or all (true) events of the process should be deleted.
   *
   * @return Number of events deleted.
   *
   * @exception JSimInvalidParametersException
   *                This exception is thrown out when the process specified is invalid and was rejected by the calendar.
   */
  protected synchronized int deleteEntriesInCalendar(final JSimProcess process, final boolean all) throws JSimInvalidParametersException
  {
    try
    {
      int  noOfDeleted;

      noOfDeleted = this.calendar.deleteEntries(process, all);
      LOG.info(noOfDeleted + " entries concerning process " + process + " have been deleted from the calendar.");
      return noOfDeleted;
    } // try
    catch (final JSimInvalidParametersException e)
    {
      LOG.warn("An error occured when deleting (an) entry(ies) from the calendar.", e);
      throw e; // Throwing the same exception to the calling function.
    } // catch
  } // deleteEntriesInCalendar

  // ------------------------------------------------------------------------------------------------------------------------------------

  /**
   * Prints a standard message or an error message. If not running in a GUI window, standard messages are printed to the standard output
   * and error messages to the error output. When running in a GUI window, all messages are printed to the user text area at the bottom of
   * the window. You should never use this method since there are methods message() and error() in the JSimProcess class.
   *
   * @param s
   *            The text message to be printed out.
   * @param code
   *            Code saying whether it is an info message (PRN_MESSAGE) or an error (PRN_ERROR).
   * @param newLine
   *            Flag indicating that the new line character should be appended to the text.
   */
  public synchronized void printString(final String s, final int code, final boolean newLine)
  {
    // If message() is called between creation of the simulation and creation
    // of the main window (first call of step()) in GUI batch mode, the GUI must be started here.
    if ((this.mode == JSimSimulationMode.GUI_BATCH) && (!this.isRunningGUI()) && (!this.mainWindowHasAlreadyExisted)) {
      this.runBatchGUI();
    }

    if (!this.isRunningGUI()) {
      switch (code)
      {
        case PRN_MESSAGE:
          if (newLine) {
            System.out.println(s);
          } else {
            System.out.print(s);
          }
          break;

        case PRN_ERROR:
          if (newLine) {
            System.err.println(s);
          } else {
            System.err.print(s);
          }
          break;

        default:
          break;
      } // switch code
    } else {
      this.mainWindow.printString(s, code, newLine);
    }
  } // printString

  /**
   * Returns true if the simulation is running in a GUI window.
   *
   * @return True if the simulation is running in a GUI window, false otherwise.
   */
  public final boolean isRunningGUI()
  {
    synchronized(this.graphicLock)
    {
      return this.windowOpen;
    } // synchronized(graphicLock)
  } // isRunningGUI

  /**
   * Opens a graphic window and lets the user control the simulation's execution using this window. Does not exit until the user presses
   * the `Quit' button. This method can be used in GUI interactive mode only.
   *
   * @return True if GUI creation was succesful, false otherwise.
   *
   * @exception JSimMethodNotSupportedException
   *                This exception is thrown out if the simulation is not running in GUI interactive mode.
   */
  public final boolean runGUI() throws JSimMethodNotSupportedException
  {
    if (this.mode != JSimSimulationMode.GUI_INTERACTIVE) {
      throw new JSimMethodNotSupportedException("JSimSimulation.runGUI(): Wrong mode. This method can be used in GUI interactive mode only.");
    }

    synchronized(this.graphicLock)
    {
      if (this.windowOpen)
      {
        LOG.warn("Unexpected inconsistency (windowOpen), exiting.");
        return false;
      }

      LOG.info("Creating the main simulation window...");

      // If something goes wrong, we don't jump to the end but handle it here.
      try
      {
        this.mainWindow = new JSimMainWindow(this, JSimSimulationMode.GUI_INTERACTIVE);
      } // try
      catch (final JSimException e)
      {
        LOG.warn("An error occured while creating the window.", e);
        this.mainWindow = null;
      } // catch

      // If we are successful, we show the window up and then wait for its closure.
      if (this.mainWindow != null)
      {
        this.mainWindow.setVisible(true);
        LOG.info("Window successfully opened, waiting for its closure.");
        this.windowOpen = true;
        this.mainWindowHasAlreadyExisted = true;
        this.guiUpdate.changed();

        // We shouldn't cycle here at all but if something goes wrong we will not run in paralell to the window.
        while (this.windowOpen)
        {
          try
          {
            LOG.info("Main thread: Sleeping.");
            this.graphicLock.wait();
          } // try
          catch (final InterruptedException e)
          {
            LOG.warn("Main thread: Interrupted while waiting for the main window's closure!", e);
            this.windowOpen = false; // We must get out
          } // catch
        } // while

        LOG.info("Woken up, destroying the window.");
        this.windowOpen = false; // This should be set by the window itself already
        if (this.mainWindow.isShowing()) {
          this.mainWindow.setVisible(false);
        }
        this.mainWindow.dispose();
        this.mainWindow = null;
        return true;
      } // if window not null

      LOG.warn("Cannot open the main window!");
      this.windowOpen = false;
      return false;
    } // synchronized(graphicLock)
  } // runGUI

  /**
   * Opens a graphic window for GUI batch mode and keeps it open. The window will exist while the step() will be called from the main
   * program. You should never call this method. It is run automatically from step(), message(), or error() when J-Sim detects that the
   * window should be opened up.
   *
   * @return True if the creation was succesful, false otherwise.
   */
  protected final boolean runBatchGUI()
  {
    synchronized(this.graphicLock)
    {
      if (this.windowOpen)
      {
        LOG.warn("Unexpected inconsistency (windowOpen), exiting.");
        return false;
      }

      LOG.info("Creating the main simulation window...");

      // If something goes wrong, we don't jump to the end but handle it here.
      try
      {
        this.mainWindow = new JSimMainWindow(this, JSimSimulationMode.GUI_BATCH);
      } // try
      catch (final JSimException e)
      {
        LOG.warn("An error occured while creating the window.", e);
        this.mainWindow = null;
      } // catch

      // If we are successful, we show the window up.
      if (this.mainWindow != null)
      {
        this.mainWindow.setVisible(true);
        LOG.info("Window successfully opened.");
        this.windowOpen = true;
        this.mainWindowHasAlreadyExisted = true;
        return true;
      }

      LOG.warn("Cannot open the main window!");
      this.windowOpen = false;
      return false;
    } // synchronized(graphicLock)
  } // runBatchGUI

  /**
   * The simulation is informed about the main window's closure. You should never use this method. Only the main window can call this
   * method although it is public. Used in GUI interactive mode.
   *
   * @param caller
   *            The main window informing about its closure.
   */
  public final void windowIsClosing(final JSimMainWindow caller)
  {
    synchronized(this.graphicLock)
    {
      if ((caller != null) && (caller == this.mainWindow)) {
        this.windowOpen = false;
      }
    } // synchronized(graphicLock)
  } // windowIsClosing

  /**
   * Suspends the calling thread until the `Quit' button of the main window is pressed. Use this method in GUI batch mode to keep the main
   * window open after the simulation is done (when step() is no longer called) to let the user see the results. The simulation need not
   * necessarily be in terminated state.
   *
   * @exception JSimMethodNotSupportedException
   *                This exception is thrown out if the simulation is not running in GUI batch mode.
   */
  public void waitForWindowClosure() throws JSimMethodNotSupportedException
  {
    if (this.mode != JSimSimulationMode.GUI_BATCH) {
      throw new JSimMethodNotSupportedException("JSimSimulation.waitForWindowClosure(): Wrong mode. This method can be used in GUI batch mode only.");
    }

    if (this.windowOpen) // We can wait only if we have something to wait for.
    {
      this.waitingForWindowClosure = true;
      this.guiUpdate.changed(); // The last GUI update
      synchronized(this.waitForWindowClosureLock)
      {
        try
        {
          LOG.info("WAITFORWINDOWCLOSURE: Waiting for the main window closure.");
          this.waitForWindowClosureLock.wait();
        } // try
        catch (final InterruptedException e)
        {
          LOG.warn("The main thread interrupted while waiting for the main window closure!", e);
        } // catch
      } // synchronized(waitForWindowClosureLock)
      this.waitingForWindowClosure = false;
      this.mainWindowHasAlreadyExisted = true;
    } // if running GUI & window open
    else {
      LOG.info("WAITFORWINDOWCLOSURE: Not waiting, the main window already destroyed or never existed.");
    }
  } // waitForWindowClosure

  /**
   * Returns true if the simulation is waiting in waitForWindowClosure().
   *
   * @return True if the simulation is waiting in waitForWindowClosure(), false otherwise.
   */
  public boolean getWaitingForWindowClosure()
  {
    return this.waitingForWindowClosure;
  } // getWaitingForWindowClosure

  // ------------------------------------------------------------------------------------------------------------------------------------

  /**
   * Prints out a text message, either to the standard output or to the simulation window.
   *
   * @param s
   *            The message to be printed out.
   */
  public void message(final String s)
  {
    this.printString(s, JSimSimulation.PRN_MESSAGE, true);
  } // message

  /**
   * Prints out a text message, either to the standard output or to the simulation window, but does not terminate the line.
   *
   * @param s
   *            The message to be printed out.
   */
  public void messageNoNL(final String s)
  {
    this.printString(s, JSimSimulation.PRN_MESSAGE, false);
  } // message

  /**
   * Prints out a text error message, either to the error output or to the simulation window.
   *
   * @param s
   *            The error message to be printed out.
   */
  public void error(final String s)
  {
    this.printString(s, JSimSimulation.PRN_ERROR, true);
  } // error

  public UUID getSimulationId() {
    return this.simulationId;
  }

  /**
   *
   */
  public void printStatistics() {
    for (final JSimProcess process : this.processes) {
      System.out.println(process);
    }
  }
} // class JSimSimulation
